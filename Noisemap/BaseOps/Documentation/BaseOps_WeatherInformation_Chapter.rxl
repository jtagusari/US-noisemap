
<section id='BaseOps_WeatherChunk_id'
webfilename='weatherinformation'>
    <title>
        Setting Weather Conditions
    </title>
    <para>
        Weather conditions are an important part of a <link
        target='#BaseOps_IntroductionToCases_id'>BaseOps case</link>.
        Temperature, relative humidity, and atmospheric pressure strongly
        influence the absorption of sound by the atmosphere. This in turn
        influences the noise levels throughout your area of interest.
    </para>
    <para>
        To set your case's weather conditions, first choose Weather from
        the <link target='#BaseOps_ListPane_id'>object type selector dropdown
        list</link>. Then set the weather conditions in the <link
        target='#WCF_ApplicationMainWindow_id'>text pane</link>.
    </para>
    <para>
        You have two choices for how to specify weather conditions.
    </para>
    <unorderedlist>
        <listitem>
            <para>
                <italic>Use these weather conditions</italic>
            </para>
            <exhibit>
                <img
                src='BaseOps_WeatherChunk_UseTheseWeatherConditions_ScreenCap.png'/>
            </exhibit>
            <para>
                Type the temperature, relative humidity, and atmospheric pressure
                that will be used when calculating noise levels.
            </para>
        </listitem>
        <listitem>
            <para>
                <italic>Pick weather conditions from monthly averages</italic>
            </para>
            <exhibit>
                <img src='BaseOps_WeatherChunk_MonthlyAverages_ScreenCap.png'/>
            </exhibit>
            <para>
                Type the average temperature, relative humidity, and atmospheric
                pressure for each month. BaseOps will then calculate the 1,000
                Hz atmospheric sound absorption coefficients for each of the 12
                months, and pick the weather conditions associated with the sixth
                smallest coefficient. An arrow <fixedtext>&lt;==</fixedtext> indicates
                the selected month.
            </para>
            <para>
                Sound absorption coefficients are calculated using the method
                given in ANSI S1.26-1995 (ASA 113-1995), "Method for Calculation
                of the Absorption of Sound by the Atmosphere". Typically, you
                will not need to concern yourself with these calculations. However,
                BaseOps provides a tool that you can use, if necessary, to manually
                calculate sound absorption coefficients. Choose <menutext>Sound
                Absorption Coefficient Tool</menutext> from the <menutext>Tools</menutext>
                menu. The Sound Absorption Coefficient Tool dialog box appears.
            </para>
            <exhibit>
                <img
                src='BaseOps_SoundAbsorptionCoefficientToolDialogBox_ScreenCap.png'/>
            </exhibit>
            <para>
                Type the desired weather conditions and sound frequency. The corresponding
                sound absorption coefficient is automatically calculated and displayed.
            </para>
        </listitem>
    </unorderedlist>
    <note type='important'>
        <para>
            Atmospheric pressures that you supply to BaseOps should <bold>not</bold>
            be sea-level corrected.
        </para>
    </note>
    <note type='note'>
        <para>
            You can select the units used to specify temperatures, atmospheric
            pressures, and sound absorption coefficients. See <iref
            target='BaseOps_CaseUnits_id'/> for more information.
        </para>
    </note>
</section>
