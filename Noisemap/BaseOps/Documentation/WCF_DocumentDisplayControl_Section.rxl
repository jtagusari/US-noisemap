
<section id='WCF_DocumentDisplayControl_id'>
    <title>
        Document Display Control
    </title>
    <para>
        A document display control displays formatted text.
    </para>
    <exhibit>
        <img src='WCF_DocumentDisplayControl_ScreenCap.png' standard_colors='true'/>
    </exhibit>
    <para>
        The following keys have special functions when using a document
        display control.
    </para>
    <exhibit>
        <table>
            <tableheader>
                <tablecolumnheader horzjust='center'>
                    Key
                </tablecolumnheader>
                <tablecolumnheader horzjust='left'>
                    Action
                </tablecolumnheader>
            </tableheader>
            <tablerow>
                <tablecell>
                    &uparrowkey;
                </tablecell>
                <tablecell>
                    Scroll up one line
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    &downarrowkey;
                </tablecell>
                <tablecell>
                    Scroll down one line
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Page Up</keytext>
                </tablecell>
                <tablecell>
                    Scroll up one page
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Page Down</keytext>
                </tablecell>
                <tablecell>
                    Scroll down one page
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Home</keytext>
                </tablecell>
                <tablecell>
                    Go to the top of the document
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>End</keytext>
                </tablecell>
                <tablecell>
                    Go to the bottom of the document
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Ctrl + P</keytext>
                </tablecell>
                <tablecell>
                    Print the document
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Ctrl + W</keytext>
                </tablecell>
                <tablecell>
                    Print preview the document
                </tablecell>
            </tablerow>
            <tablerow>
                <tablecell>
                    <keytext>Ctrl + H</keytext>
                </tablecell>
                <tablecell>
                    Save the document as HTML
                </tablecell>
            </tablerow>
        </table>
    </exhibit>
    <para>
        If your mouse has a middle mouse button, you can use it to scroll
        the document. Press and hold the middle button, then move the
        mouse up or down.
    </para>
    <para>
        If your mouse has a wheel button, you can scroll the document
        by turning the wheel.
    </para>
</section>

