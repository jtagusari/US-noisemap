
[-defdefine TermUsedForMap "map"-]
[-defdefine TermUsedForMap_FirstLetterUppercase "Map"-]
[-defdefine TermUsedForMap_InitialLettersUppercase "Map"-]
[-defdefine TermUsedForMap_Collective "your &TermUsedForMap;s"-]
[-defdefine ChapterTitle "Changing the &TermUsedForMap_InitialLettersUppercase; Background"-]
[-defdefine BackgroundIntro_ExtraText ""-]
[-defdefine MapBackground_Layers_Intro
        "&app_name; can display &TermUsedForMap_Collective; with one or more background layers.
        The layers can be stored in a number of common Geographic
        Information System (GIS) formats. These formats are discussed
        in detail in <iref target='WCGIS_BackgroundMapFormats_id'/>."-]
[-defdefine	MapOptionsDialogBoxId "&app_name;_&TermUsedForMap_InitialLettersUppercase;OptionsDialogBox_id"-]
[-defdefine LinkToMapOptionsDialogBox "<link target='#&MapOptionsDialogBoxId;'>&TermUsedForMap_InitialLettersUppercase; Options dialog box</link>"-]
[-defdefine MapBackground_Layers_SelectMapLayerType_ScreenCapFile "WCGIS_MapBackground_Layers_SelectMapLayerType_ScreenCap.png"-]
[-defdefine MapBackground_Layers_DescriptionOfWhereLayersAreDescribed_ExtraPrefixText ""-]
[-defdefine MapBackground_Layers_ExtraChapterSuffixText ""-]
<section id='WCGIS_MapBackground_id'
webfilename='mapbackground'>
    <title>
        &ChapterTitle;
    </title>
    <para>
        &app_name; gives you considerable control over the background
        of &TermUsedForMap_Collective;. It can be as simple as a solid color, or
        as complicated as a set of map layers displaying themes such as
        roads and rivers. &BackgroundIntro_ExtraText;
    </para>
    <section id='WCGIS_MapBackground_Color_id'>
        <title>
            Background Color
        </title>
        <para>
            When &app_name; draws a &TermUsedForMap;, it first fills the entire
            &TermUsedForMap; with the background color. To set the background
            color, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Go to the Background Color page of the &LinkToMapOptionsDialogBox;.
                </para>
                <exhibit>
                    <img src='WCGIS_MapBackground_BackgroundColor_ScreenCap.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    <italic>Background color:</italic> Select the background color.
                    See <iref target='WCF_ColorControl_id'/> for information on selecting
                    colors.
                </para>
            </listitem>
        </orderedlist>
    </section>
    <section id='WCGIS_MapBackground_Layers_id'>
        <title>
            Background Layers
        </title>
        <para>
            &MapBackground_Layers_Intro;
        </para>
        <section>
            <title>
                Managing Background Layers
            </title>
            <para>
                Use the Background Layers page of the &LinkToMapOptionsDialogBox;
                to manage background layers.
            </para>
            <exhibit>
                <img src='WCGIS_MapBackground_BackgroundLayers_ScreenCap.png'/>
            </exhibit>
            <para>
                Check the <italic>Show background layers</italic> box to display
                background layers. If this box is not checked, no layers will
                be shown.
            </para>
            <para>
                The <termtext>background layers list</termtext> displays all background
                layers. This list is presented using a spreadsheet control.
                See <iref target='WCF_SpreadsheetControl_id'/> for more information.
            </para>
        </section>
        <section id='WCGIS_MapBackground_Layers_Adding_id'>
            <title>
                Adding a Background Layer
            </title>
            <para>
                To add a background layer, follow these steps.
            </para>
            <orderedlist>
                <listitem>
                    <para>
                        If desired, select an existing layer by clicking on it. The new
                        layer will be added just before the selected layer. Ignore this
                        step if the list is empty.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        Press the Add Layer button, located below the background layers
                        list. The Select Layer Type dialog box is displayed.
                    </para>
                    <exhibit>
                        <img
                        src='&MapBackground_Layers_SelectMapLayerType_ScreenCapFile;'/>
                    </exhibit>
                </listitem>
                <listitem>
                    <para>
                        Choose the type of layer you would like to add, then press OK.
                        As a shortcut, simply double-click on the desired layer type.
                    </para>
                    <para>
                        &MapBackground_Layers_DescriptionOfWhereLayersAreDescribed_ExtraPrefixText;
                        The background map layer types are described in <iref
                        target='WCGIS_BackgroundMapFormats_id'/>.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        A new layer, of the appropriate type, is added to the list.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        Press the new layer's Options button. The layer's options dialog
                        box appears. Set the layer options as desired.
                    </para>
                </listitem>
            </orderedlist>
        </section>
        <section>
            <title>
                Removing a Background Layer
            </title>
            <para>
                To remove a background layer, follow these steps.
            </para>
            <orderedlist>
                <listitem>
                    <para>
                        Select the layer to remove by clicking on it in the background
                        layers list.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        Press the Remove Layer button, located below the background layers
                        list.
                    </para>
                </listitem>
            </orderedlist>
        </section>
        <section>
            <title>
                Ordering Background Layers
            </title>
            <para>
                Background layers are drawn in order, starting with the last
                layer in the background layers list. This means that the topmost
                layer in the list is drawn last, and therefore appears on top
                of other layers. Generally, you will want to order the list so
                that the most important layers are at the top.
            </para>
            <note type='note'>
                <para>
                    Background layers are drawn in three stages. All area features
                    are drawn first, followed by all line features, and finally all
                    point features. This means that a filled area feature will not
                    obscure a point feature, regardless of the ordering of the layers.
                    This matches most people's intuitive idea of how layers should
                    work.
                </para>
            </note>
            <para>
                To change the ordering of layers in the list, follow these steps.
            </para>
            <orderedlist>
                <listitem>
                    <para>
                        Select a layer by clicking on it in the background layers list.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        Press the Move Up In List or Move Down In List button to move
                        the selected layer. These buttons are located just below the background
                        layers list.
                    </para>
                    <para>
                        If you press and hold one of these button, it will automatically
                        repeat. This is useful for moving a layer several rows up or down.
                    </para>
                </listitem>
            </orderedlist>
        </section>
        <section>
            <title>
                Hiding or Displaying a Background Layer
            </title>
            <para>
                Use the Show column of the background layers list to selectively
                hide or display individual layers. A layer will be displayed only
                if its Show box is checked.
            </para>
        </section>
        <section id='WCGIS_MapBackground_Layers_ChangingOptions_id'>
            <title>
                Changing a Background Layer's Options
            </title>
            <para>
                Use the Options column of the background layers list to change
                the options used to display a layer. Pressing a layer's Options
                button will display that layer's options dialog box.
            </para>
        </section>
    </section>
    &MapBackground_Layers_ExtraChapterSuffixText;
</section>
