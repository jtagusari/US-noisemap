
[-defdefine LessonName "Scaling the Number of Flight Operations"-]
<section id='BaseOps_Tutorial_ScalingOperations_id'>
    [-include "BaseOps_SetUpStandardWorkbookCase.tutscript"-]
    <title>
        &LessonName;
    </title>
    <section>
        <title>
            &LessonName;
        </title>
        <para>
            As a base planner, you may be asked to determine how your installation's
            noise contours will be affected by a change in the number of flight
            operations. For example, your installation may be considering
            increasing the number of F-16A closed patterns by 10%, and you
            may be asked to determine if this will cause a significant increase
            in the noise contours.
        </para>
        <para>
            Using BaseOps' scenario feature, you can easily determine the
            impact of such a change. This lesson will teach you how to do
            this. It is presented as a list of step-by-step instructions.
            To begin, press the <bold>First Step</bold> button below.
        </para>
    </section>
    <section>
        <title>
            Step 1
        </title>
        <para>
            Go to the object type selector dropdown list in the upper-left
            corner of the BaseOps window, and select "Scenarios".
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 1.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 2
        </title>
        <para>
            Every BaseOps case contains one or more <termtext>scenarios</termtext>.
            A scenario is a set of case modifications, such as scaling flight
            profile operations or deleting static profiles. When you run the
            case, a noise analysis is performed for each scenario.
        </para>
        <para>
            When a new case is created, it contains a single scenario named
            "Baseline". This baseline scenario does not modify the case. In
            many situations, this is the only scenario required.
        </para>
        <para>
            However, you may sometimes need to perform a noise analysis for
            two or more situations, to compare the noise impact of each alternative.
            In this tutorial, we want to perform a noise analysis with F-16A
            closed patterns operations increased by 10%, and then compare
            it against the baseline scenario.
        </para>
        <para>
            To create the new scenario, press the Add Scenario button <img
            src='intres:WCF_AddPlusSignToolbarButtonBitmap'/>, located above
            the list on the left side of the BaseOps window.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 2.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 3
        </title>
        <para>
            Give the new scenario a descriptive name, such as "Increase F-16
            patterns by 10%".
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 3.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 4
        </title>
        <para>
            Press the Add New Change button <img
            src='intres:WCF_AddPlusSignToolbarButtonBitmap'/>, located in
            the lower-right portion of the BaseOps window. A dialog box is
            displayed, asking you to select the type of change to make to
            the BaseOps case.
        </para>
        <exhibit>
            <img
            src='BaseOps_ScalingOperations_TypeOfChangeDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            Click on "Scale Flight Profile Operations", the press OK.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 4.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 5
        </title>
        <para>
            When first added, the change defaults to multiplying all flight
            operations by 1. We will need to edit this change so that it multiplies
            the number of F-16A closed pattern operations by 1.1 (which in
            equivalent to an increase of 10%). To edit the change, click on
            the Edit button <img src='BaseOps_ThreeDotsButton_Figure.png'/>,
            located to the right of the new change you just added.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 5.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 6
        </title>
        <para>
            The "Scale Flight Profile Operations" dialog box appears.
        </para>
        <exhibit>
            <img
            src='BaseOps_ScalingOperations_ScaleFlightProfileOperationsDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            In the "Multiply Number of Day Operations by" and "Multiply Number
            of Night Operations by" boxes, type 1.1.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 6.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 7
        </title>
        <para>
            Click on the radio button labeled "Scale only the flight profiles
            that match the following conditions". For "Flight Track Types",
            click on the Edit button <img
            src='BaseOps_ThreeDotsButton_Figure.png'/>, then choose only closed
            patterns. For "Aircraft Names", choose only F16A. Then press OK.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 7.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 8
        </title>
        <para>
            Run the case by choosing <menutext>Run Case</menutext> from the
            <menutext>Case</menutext> menu. The NMap noise model will be run
            twice: once for the baseline scenario, and once for our new scenario
            that increases F-16A closed patterns by 10%.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 8.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 9
        </title>
        <para>
            To see the noise contours for our new scenario, choose <menutext>Plot</menutext>
            from the <menutext>Case</menutext> menu. A dialog box will appear,
            asking you which scenario's contours you wish to view.
        </para>
        <exhibit>
            <img
            src='BaseOps_ScalingOperations_SelectGridToPlotDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            Click on "Increase F-16 patterns by 10% Scenario", then press
            OK. The NMPlot plotting software will open, showing you what your
            installation's noise contours would look like if you increased
            the F-16A closed pattern operations by 10%. You can compare these
            against your existing baseline noise contours to determine if
            there are significant differences.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ScalingOperations - 9.wcsaf'-]
        </para>
    </section>
</section>
