
[-defdefine LessonName "Getting Help"-]
<section id='BaseOps_Tutorial_GettingHelp_id'>
    [-include "BaseOps_SetUpStandardWorkbookCase.tutscript"-]
    <title>
        &LessonName;
    </title>
    <section>
        <title>
            &LessonName;
        </title>
        <para>
            The lessons in this tutorial series are intended to give you an
            overview of how to use BaseOps to perform some common tasks. However,
            the tutorial can't cover every detail. When working with your
            installation's case, it is likely that questions and complications
            will arise.
        </para>
        <para>
            This lesson will teach you how to get help while working with
            your installation's BaseOps case. It is presented as a list of
            step-by-step instructions. To begin, press the <bold>First Step</bold>
            button below.
        </para>
    </section>
    <section>
        <title>
            Step 1
        </title>
        <para>
            BaseOps has an extensive online help system. This help is context-sensitive,
            meaning that when you request help, the topics most relevant to
            your current location in the BaseOps application are displayed.
        </para>
        <para>
            To get help, either press the <keytext>F1</keytext> key, or else
            choose <menutext>Relevant Topics</menutext> from the <menutext>Help</menutext>
            menu.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 1.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 2
        </title>
        <para>
            The "BaseOps User's Guide" dialog box is displayed.
        </para>
        <exhibit>
            <img src='BaseOps_GettingHelp_HelpDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            A list of help topics is displayed on the left side of the dialog
            box. These topics are sorted by their relevance to your current
            location in the BaseOps application. Click on a topic name, and
            it is displayed on the right side of the dialog box.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 2.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 3
        </title>
        <para>
            To determine the function of a particular button or control, briefly
            hold the mouse cursor over it. A small tooltip window will appear,
            giving you a brief description.
        </para>
        <note type='note'>
            <para>
                Not all controls and buttons have associated tooltip windows.
            </para>
        </note>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 3.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 4
        </title>
        <para>
            To get more extensive help about a particular button or control,
            either press <keytext>Shift+F1</keytext>, or else choose <menutext>What's
            This</menutext> from the <menutext>Help</menutext> menu. Then
            click on the button or control. The relevant portion of the User's
            Guide will be displayed.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 4.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 5
        </title>
        <para>
            If you want to browse the BaseOps User's Guide, choose <menutext>Contents</menutext>
            from the <menutext>Help</menutext> menu. Each chapter in the User's
            Guide is displayed on the left side of the dialog box. Click on
            a chapter, and it is displayed.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 5.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 6
        </title>
        <para>
            The NMPlot plotting application contains a help system that works
            just like the one in BaseOps.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 6.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 7
        </title>
        <para>
            If you have questions about the data in your installation's BaseOps
            case, you should contact the person or organization that created
            the case file. To find this contact information, go to the object
            type selector dropdown list in the upper-left corner of the BaseOps
            window, and select "Case".
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 7.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 8
        </title>
        <para>
            Scroll the bottom right portion of the BaseOps window until you
            can see the contact information associated with the case.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_GettingHelp - 8.wcsaf'-]
        </para>
    </section>
</section>
