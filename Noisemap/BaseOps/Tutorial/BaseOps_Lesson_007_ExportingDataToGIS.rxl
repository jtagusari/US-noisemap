
[-defdefine LessonName "Exporting Data to a GIS"-]
<section id='BaseOps_Tutorial_ExportingDataToGIS_id'>
    [-include "BaseOps_SetUpStandardWorkbookCase.tutscript"-]
    <title>
        &LessonName;
    </title>
    <section>
        <title>
            &LessonName;
        </title>
        <para>
            It is sometimes useful to export BaseOps case data (noise contours,
            flight tracks, etc.) to a geographic information system (GIS)
            such as Arc/Info or GRASS. The GIS can then be used to make maps
            and perform analysis that are beyond the capabilities of Noisemap.
        </para>
        <para>
            This lesson will teach you how to export data in a form that can
            be read by a GIS. It is presented as a list of step-by-step instructions.
            To begin, press the <bold>First Step</bold> button below.
        </para>
    </section>
    <section>
        <title>
            Step 1
        </title>
        <para>
            If all you need to export are noise contours, then BaseOps can
            do that on its own. Go to the object type selector dropdown list
            in the upper-left corner of the BaseOps window, and select "Contours".
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 1.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 2
        </title>
        <para>
            In the box labeled "Contour Levels", type in the contour noise
            levels of interest, separated by commas.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 2.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 3
        </title>
        <para>
            There are numerous geographic coordinate systems (often referred
            to as projections) that geographic data can be stored in. Examples
            include Longitude/Latitude and UTM. A coordinate system may have
            various numeric options: for example, the UTM coordinate system
            has a option called the zone number. Finally, each coordinate
            system has an associated datum. Examples of datums include WGS-84
            and NAD-27.
        </para>
        <para>
            You must decide which coordinate system you will export the data
            in. You should decide on a coordinate system in conjunction with
            the person(s) who will use the exported data. You may want to
            contact your local GIS expert for advice on choosing a coordinate
            system.
        </para>
        <note type='tip'>
            <para>
                If you don't know what coordinate system to use, a reasonable
                default is to select Longitude and Latitude, with a datum of WGS-84.
                Most GIS software should be able to import data in that coordinate
                system.
            </para>
        </note>
        <para>
            For this example, we will assume that the export coordinate system
            is UTM, zone 16, with a datum of WGS-84. To choose the coordinate
            system , press the edit button <img
            src='BaseOps_ThreeDotsButton_Figure.png'/> to the right of the
            box labeled "Coordinate System". The dialog box for choosing a
            coordinate system is displayed.
        </para>
        <exhibit>
            <img
            src='BaseOps_ExportingDataToGIS_SelectCoordinateSystemDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            Choose "UTM" from the dropdown list at the top, then choose 16
            from the dropdown list labeled "Zone". Then press OK.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 3.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 4
        </title>
        <para>
            Run the case by choosing <menutext>Run Case</menutext> from the
            <menutext>Case</menutext> menu.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 4.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 5
        </title>
        <para>
            As part of running the case, Noisemap will have created shapefiles
            containing the contour lines. Shapefiles are a common GIS file
            format for storing geographic data. Most GISs can read shapefiles.
        </para>
        <para>
            The shapefiles will be located in the same directory as your <filetext>.baseops</filetext>
            case file. There should be three files, ending with the text <filetext>Contours.dbf</filetext>,
            <filetext>Contours.shp</filetext>, and <filetext>Contours.shx</filetext>.
            These three files constitute the shapefile data.
        </para>
        <para>
            There will also be a fourth file, ending with the text <filetext>Contours.xml</filetext>.
            This file contains metadata conforming to the Federal Geographic
            Data Committee's Content Standard for Digital Geospatial Metadata
            (FGDC-STD-001-1998). If you send the shapefile data to a third party,
            you should also send this metadata file, as it contains information
            that will help other parties interpret the shapefile data .
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 5.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 6
        </title>
        <para>
            If you need to export data other than the contour lines (for example,
            flight tracks), then you will need to use the NMPlot application
            to do so. Run NMPlot by choosing <menutext>Plot</menutext> from
            the <menutext>Case</menutext> menu.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 6.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 7
        </title>
        <para>
            NMPlot will export only the data that is being displayed on the
            screen. Assume that we want to export flight tracks. First, we
            must display the flight tracks in NMPlot.
        </para>
        <para>
            Choose <menutext>Options</menutext> from NMPlot's <menutext>Plot</menutext>
            menu.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 7.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 8
        </title>
        <para>
            The plot options dialog box is displayed.
        </para>
        <exhibit>
            <img
            src='BaseOps_ExportingDataToGIS_NMPlotLineOptionsDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            In the list of option categories on the left side of the dialog
            box, click on Lines. Then, in the grid control on the right side
            of the dialog box, find the "Flight Track" row, and check the
            box in the "Show" column. Finally, press OK.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 8.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 9
        </title>
        <para>
            Flight tracks are now displayed on the plot. We are ready to export
            them to the GIS.
        </para>
        <para>
            Choose <menutext>Export to GIS</menutext> from NMPlot's <menutext>File</menutext>
            menu. The "Export to GIS" dialog box will be displayed.
        </para>
        <exhibit>
            <img
            src='BaseOps_ExportingDataToGIS_ExportToGISDialogBox_ScreenCap.png'/>
        </exhibit>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 9.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 10
        </title>
        <para>
            In the dropdown list labeled Format, choose "ESRI ARC/INFO Shapefile
            (SHP)".
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 10.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 11
        </title>
        <para>
            Set the export coordinate system. Press the "Properties" button.
            The export properties dialog box will appear.
        </para>
        <para>
            In the list on the left side of the dialog box, click on "Coordinate
            System". Then, on the right side of the dialog box, choose the
            coordinate system in the same way as you did earlier in this tutorial
            (recall that we are using UTM, zone 16). Finally, press OK.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 11.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 12
        </title>
        <para>
            Insure that the radio button labeled "Create based on current
            document file name" is selected. Then press OK. The data will
            be exported.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 12.wcsaf'-]
        </para>
    </section>
    <section>
        <title>
            Step 13
        </title>
        <para>
            The shapefiles will be located in the same directory as your <filetext>.baseops</filetext>
            case file. There should be groups of three files, ending with
            the text <filetext>.dbf</filetext>, <filetext>.shp</filetext>,
            and <filetext>.shx</filetext>.
        </para>
        <para>
            [-include "BaseOps_ShowMeHowLink.rxl" VideoName='BaseOps_ExportingDataToGIS - 13.wcsaf'-]
        </para>
    </section>
</section>
