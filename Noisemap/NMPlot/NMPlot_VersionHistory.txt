
                          NMPlot Version History




Version 4.974

    - NMPlot is now a 64-bit application. This allows it to use
      more of your computer's memory, allowing the application
      to load larger grids and display larger background maps.

    - NMPlot is now compiled using the Clang optimizing compiler,
      resulting the the application running significantly faster.




Version 4.973

    - NMPlot now runs correctly on computer systems with more than
      one screen/monitor.

      There is one restriction: all monitors must have the same
      display resolution (i.e., the same DPI/PPI/pixels per inch).

    - The standard Microsoft Windows color selection dialog box has
      been replaced with a custom version that supports the latest
      advances in color theory.  See the "Color Control" section of
      the "Details of Various NMPlot Components" chapter in the
      NMPlot User's Guide for more information.




Version 4.972

    - When labeling line geographic annotations of category
      "Runway", labels are now positioned at the beginning
      of the line, following the convention for how runways
      are typically labeled on aviation charts.

    - NMPlot now supports high-PPI (high pixel per inch, high
      resolution) monitors.




Version 4.971

    - When exporting vector-based GIS data, NMPlot now produces
      a .prj file that describes the coordinate system of the
      exported data.




Version 4.970

    - NMPlot can now process bitmap images in GIF format.




Version 4.969

    - The "Grid Computation" and "Point of Interest Computation"
      mouse tools now support grids created using the MRNMap noise
      model.




Version 4.968

    - NMPlot now supports the new point-of-interest file format used
      by the latest versions of NMap and AAM.

    - The "Grid Computation" and "Point of Interest Computation"
      mouse tools now work for grids created from combined NMap/AAM
      BaseOps cases.  When using these tools on combined grids,
      NMPlot will run both noise models and combine the resulting
      output files as necessary.




Version 4.967

    - By popular demand, the method of panning a plot with the
      mouse has been changed.  The old 'center on click' pan tool
      has been replaced with a 'grabbing hand' pan tool.  This
      bring NMPlot into line with how panning is done in most
      other software.

    - When creating color gradient plots, a new color interpolation
      space is now available: CIE-LAB (International Commission on
      Illumination L*a*b*, 2 degree observer, D65 illuminant).

      The CIE-LAB color space is more perceptually uniform than the
      other color spaces supported by NMPlot (RGB and HLS), and
      therefore will often produce more pleasing results.

      See the "Color Gradient Plots" chapter of the NMPlot User's
      Guide for more information.




Version 4.966

    - Fixed bug that could cause NMPlot to display incorrect
      point-of-interest data for a noise grid created by NMap.




Version 4.965

    - The "Grid Computation" and "Point of Interest Computation"
      mouse tools now work with grids created by the AAM noise
      model.




Version 4.964

    - Fixed bug that could occur when combining multiple grids
      created by INM.  If this bug was triggered, NMPlot would
      enter an endless loop and never finish combining the grids.




Version 4.963

    - Modified NMPlot to gracefully handle corrupt shapefiles.




Version 4.962

    - NMPlot now supports bitmap background maps in the Albers
      Conical Equal Area projection.




Version 4.961

    - Fixed bug that could result in exported shapefiles with
      incorrect bounding box information.




Version 4.96

    - Color opacity support has been added.  When filling contours,
      drawing color gradient plots, and filling vector map areas,
      you can now specify a percent opacity.  The opacity may range
      from 0% (totally transparent) to 100% (totally opaque).
      Intermediate opacities will tint areas with a translucent
      color that allows the background (for example, a background
      map) to show through.

    - NMPlot now provides a tool that streamlines the process of
      combining a large number of grids.  To access it, choose
      "Multiple Grid Combining Tool" from the "Tools" menu.

      See the "Working With Grids" chapter of the NMPlot User's
      Guide for more information.

    - You can now select the units (feet, miles, meters, etc.) used
      when drawing a graphical map scale on a map.  To select the
      units, choose "Application Options" from the "Options" menu,
      then go to the "Physical Units" page.

    - The way errors are handled has changed when running NMPlot
      with the -se, -ExportToDXF, and -pi command-line options.
      Instead of being reported using a pop-up error message dialog
      box, errors are now written to an error text file.  Calling
      scripts or applications can check for the existence of this
      file to determine if an error occurred.

      See "Batch Mode Error Handling", in the "Command-Line Options"
      appendix of the NMPlot User's Guide, for a full discussion.

    - Windows XP or Vista is now required to run NMPlot.  The
      Windows 95, Windows NT, and Windows 2000 operating systems
      are no longer supported.




Version 4.958

    - Added the ability to control how a contour line appears when
      it reaches the edge of the grid.  Previously, it followed the
      grid's edge to the location where the contour reentered.  Now,
      you can choose to simply clip the contour at the grid edge.
      See the "Contours / Clipping" page of the "Plot Options"
      dialog box.




Version 4.957

    - Fixed minor bug: .coordsys extension not appended to file name
      when saving a coordinate system.

    - Changed number of digits of precision used when labeling contours
      with their numeric levels.




Version 4.956

    - Fixed bug: NMPlot would crash when displaying a color gradient
      plot.




Version 4.955

    - Fixed bug: When displaying a NOISEMAP point-of-interest report,
      NMPlot would always label the noise metric as DNL, even if the
      report was in another metric such as CNEL.




Version 4.954

    - Fixed bug: NMPlot would crash when computer was connected to a video
      projector.




Version 4.953

    - NMPlot can now read a georeferenced bitmap map layer whose image is stored
      in Windows Bitmap (.BMP) format with a color depth of 1 bit per pixel.
      Previously, only color depths of 8 and 24 bits per pixel were supported.

    - Improved error messages when reading ESRI ARC/INFO world files.




Version 4.952

    - When exporting a plot to an ARC/INFO Shapefile, previous versions of
      NMPlot write each contour's level and level units as Shapefile attributes.
      Geographical information systems, such as ARC/INFO, can read these
      attributes when importing the Shapefile.

      This version of NMPlot includes the following additional attributes:

          - each contour's area

          - the units used to specify contour areas, as set on the "Physical
            Units" page of the "NMPlot Application Options" dialog box

          - the contour topology (either "Overlapping Polygons" or "Adjacent
            Nested Rings"), as set on the "Topology" page of the "Plot Options"
            dialog box




Version 4.951

    - NMPlot can now import a wider range of DXF background map files.




Version 4.95

    - Full screen mode is now supported.  In full screen mode, NMPlot's main
      window expands to fill the screen.  Various elements, such as the title
      bar, the main menu, and the Microsoft Windows task bar, are hidden.  Use
      full screen mode to gain the maximum possible display area for your plots.

      To enter full screen mode, press Ctrl+F11, or select "Full Screen" from
      the "Edit" menu.  To leave full screen mode, press Ctrl+F11, or press the
      "Exit Full Screen" button on the toolbar.

      Even though the main menu is hidden in full screen mode, you can access it
      using keyboard shortcuts (for example, Alt+F to access the "File" menu).

      Even though the task bar is hidden in full screen mode, you can switch to
      another application using Alt+Tab.




Version 4.94

    - Fixed bug: Fatal NMPlot error while triangulating some grids.




Version 4.93

    - Corrected bug that would cause NMPlot to crash with a "Not enough memory"
      error after using print preview for a long time.

    - Partially fixed bug where a bitmap background map layer disappears if you
      zoom in too far.  This bug has been fixed for Windows XP, 2000, and NT
      systems.

      For Windows 95, 98 and Me systems, this bug would be quite expensive to
      fix.  Therefore, at this time, there are no plans to correct it on these
      systems.




Version 4.92

    - NMPlot now uses an improved method for mapping grid XY coordinates to
      geographic locations.  The new method is more accurate, especially at
      higher latitudes.

    - NMPlot now supports bitmap background maps in the following projections:
        - Lambert Azimuthal Equal Area
        - Stereographic




Version 4.91

    - Fixed bug: Error message when displaying a bitmap map layer on some
      Windows 95/98/ME machines.

    - Fixed bug: Fatal error would sometimes occur when contour topology was
      set to "Adjacent Nested Rings".




Version 4.90

    - Fixed bug: Fatal error when loading a grid that contains an ampersand "&"
      in its file name.




Version 4.89

    - Fixed bug: Fatal error when using mouse to pan or zoom a plot.




Version 4.88

    - Corrected a bug that caused an error message when displaying the Open
      File dialog box under Windows NT 4.0.




Version 4.87

    - Corrected a bug that caused NMPlot to use excessive memory if several
      large grids files were opened and then closed in succession.




Version 4.86

    - When exporting plots to a GIS in a format that supports layers (such as
      ESRI Shapefiles), grid data points are now exported in their own layer,
      named "Grid Data Points".

    - When exporting plots to a GIS in a format that supports attributes (such
      as ESRI Shapefile), grid data points now have an attribute labeled
      "Value", which contains the grid points' data values.




Version 4.85

    - The "Point Of Interest Computation" and "Grid Computation" tools have
      been modified to work with grids generated by Noisemap/BaseOps version
      7.21.




Version 4.84

    - Fixed bug that occurred when scrolling plots using middle mouse button.




Version 4.83

    - New command-line options are supported.  See the "Command-Line Options"
      chapter of the User's Guide for more information.

          - Export a plot in ARC/INFO Shapefile (SHP) Format

          - Interpolate Grid Data Levels at a List of User-Supplied Points




Version 4.82

    - When exporting to a GIS in ESRI Shapefile format, NMPlot creates .dbf
      attribute tables.  The format of these tables has been changed so that
      they are compatible with ArcView.

    - Fixed bug in which NMPlot would sometimes crash when a bitmap map layer
      in a Long/Lat projection was displayed.




Version 4.81

    - Option added to tweak the location of a CADRG background map.




Version 4.8

    - Any number of contour styles (color, line width, etc) can now be defined.
      This lets you create, for example, a plot with 5 contours, each filled
      with a different color.  See the "Contour Plots" chapter in the User's
      Guide for more information.

    - Improved plot legends when printing.  Legends can now have multiple fonts,
      colors, and borders.  Text can be segregated into outlined rectangular
      areas, much like an architectural blueprint legend.  A graphical scale and
      north arrow are supported.  Legends can be saved to and loaded from files.
      Several prebuilt legends are included with NMPlot.  See the "Printing
      Plots" chapter of the User's Guide for more information.

    - NMPlot can now use world files to georeference bitmap image maps. See the
      section titled "Georeferenced Bitmap", in the "Background Map Formats"
      chapter of the User's Guide, for more information.

    - You can now save and load geographic coordinate systems to/from files.

    - Can now easily create a plot that only displays a background map.  No
      grid file is necessary.  See the section titled "Creating a New Plot",
      in the "Introduction to Plots" chapter of the User's Guide, for more
      information.

    - Can now set home view to include background maps.  See the section titled
      "The Home View", in the "Working With Plots" chapter of the User's Guide,
      for more information.

    - Improved animation support.  RLE compression of movies now supported.
      Movies are now playable on a greater range of platforms.

    - When defining how a grid's geographic annotations should be displayed on a
      plot, you can now select from the available categories and names.  See the
      section titled "Editing Rule Tables", in the "Displaying Geographic
      Annotations" chapter of the User's Guide, for more information.

    - You can now use any character from any font as a map symbol.  See the
      section titled "Symbol Control", in the "Details of Various NMPlot
      Components" chapter of the User's Guide, for more information.

    - You can choose to display every n'th grid data point on a plot.  See the
      "Displaying Grid Data Points" chapter in the User's Guide for more
      information.

    - Can now print preview plots.

    - Menu command added that displays the primary grid of a plot.  Choose
      "Display Primary Grid of this Plot" from the "Plot" menu.

    - When adding a Digital Line Graph (DLG) map layer, you can restrict which
      geographic features are displayed.  You can select any combination of
      point, line, and area features.

    - Optimization: When processing a grid file that consists of a single GRID
      section, NMPlot uses alternative interpolation and contouring methods that
      are much faster, and uses much less memory.  See the section titled
      "Simple Rectangular Grid Optimization", in the "Introduction to the Noise
      Model Grid Format" chapter of the User's Guide, for more information.

    - Fixed a bug that caused a bitmap image map layer to be displayed in the
      wrong location when using a projection that used units other than meters.

    - The NMGF grid format now supports Universal Transverse Mecator (UTM)
      coordinates.  NMPlot can read UTM grid files.

    - When exporting a plot to a ShapeFile, additional attributes are exported.
      Specifically, any string, integer, and float attributes associated with
      geographic annotations are exported.




Version 4.7

    - Animation support has been added.  NMPlot can now create a movie from a
      series of grid files.  See the new chapter, "Creating Movies", in the
      User's Guide.

    - Added support for background maps in Compressed ARC Digitized Raster
      Graphics (CADRG) format.

    - Support for geographic datums added.  See the new appendix in the User's
      Guide, titled "Introduction to Datums", for information.

    - Method of georeferencing bitmap image map layers improved.  NMPlot now
      supports georeferenced bitmap images in a variety of common projections.

    - Tool added for converting between geographic coordinate systems.  Choose
      "Coordinate System Conversion Tool" from the "Tools" menu.

    - Improved north arrow displayed in upper-right corner of the NMPlot window.

    - NMPlot now asks the operating system for the colors used for dialog boxes,
      buttons, etc.  If you have customized the system colors on your computer,
      NMPlot will now use them.




Version 4.6

    - A user's guide and online help system have been added.  Access both
      through the Help menu.

    - A summary view for grids has been added.  Choose Open from the File menu,
      open a grid (.grd) file, then click on the Summary tab.

    - When creating a new plot, user is prompted for the name of the grid file
      to display in the plot.

    - The set of sample files shipped with NMPlot has been changed.  Files have
      been renamed to make them easier for users to understand.

    - Several small changes have been made to the wording used in NMPlot's user
      interface.  The terminology used is now more consistent and easier to
      understand.

    - The order in which a plot's background map layers are drawn has been
      reversed.  Layers are now drawn starting with the last layer in the list.
      This makes the top layer in the list appear on top of the other layers in
      the plot.




Version 4.5

    - Can copy plot to clipboard.

    - Can export plot to a bitmap file.  The following formats are supported:
      BMP, JPG, TIF, and PNG.  Select "Export to Bitmap" from the File menu.

    - When exporting plot to clipboard or bitmap file, plot can be oversampled,
      which results in a higher quality image.

    - Can import background maps in AutoCAD Drawing Exchange Format (DXF) format.
      The following DXF entity types are supported: POINT, LINE, and POLYLINE.

    - Static pad symbol rotates to show direction of aircraft.

    - Back by popular demand: the aircraft symbol used to represent static pads.




Version 4.41

    - Fixed bug in which program would enter infinite redrawing loop when
      displaying very complex plots on the screen.




Version 4.4

    - Can export plots in ESRI ARC/INFO Shapefile (.SHP) format.  When viewing
      plot, select "Export to GIS" from the File menu, then select "ESRI
      ARC/INFO Shapefile (SHP)" as the output format.

    - Can display background maps stored as georeferenced bitmap images.  From
      plot options dialog box, select Overlays category, click on "Add Layer"
      button, then choose "Georeferenced Bitmap Image" for layer type.

      The following bitmap formats supported:
          BMP (1, 4, 8, 16, 24, & 32-bit)
          JPG (8 & 24-bit)
          PNG (1, 4, 8, 16, 24, 32, & 64-bit)
          TIF (1, 8, & 24-bit, uncompressed)

          Note: Geotiff format not currently supported.  Currently,
          georeferencing information must be entered manually.

    - Fixed bug in which color gradient plots were not being clipped properly.




Version 4.3

    - Can display background maps stored in ESRI ARC/INFO Shapefile (.SHP)
      format.

    - Noisemap-specific extensions.  Can now run Noisemap (the US Air Force's
      aircraft noise model) and calculate grids and points of interest from
      within NMPlot.

    - The coordinate system used to display the mouse cursor location on the
      toolbar can be selected.  Choices are Long/Lat (decimal or deg-min-sec),
      UTM, and flat-earth X-Y Cartesian.  Select "Application Options" from
      the Options menu, then select the "Mouse Display" pane.

    - Additional choices for the output coordinate system when exporting to a
      GIS: UTM and flat-earth X-Y Cartesian.  Choose "Export to GIS" from the
      File menu, press the Properties button, then select "Coordinate System"
      pane.

    - When measuring distances with the Measurement tool, the distance units
      (feet, meters, miles, etc) can be selected.  Select "Application Options"
      from the Options menu, then select the "Physical Units" pane.

    - When exporting to a GIS, the level of detail can be selected.  Choose
      "Export to GIS" from the File menu, press the Properties button, then
      select the "Level of Detail" pane.

    - When printing, the level of detail can be selected.  Choose "Print
      options" from the File menu, then select the "Level of Detail" pane.

    - When combining grids, can select the distance used for testing if two
      grid points have essentially the same location.  Choose "Combine Data
      Points" from the Grid menu.

    - When exporting in DXF format, layer name of a contour can include the
      contour level.  Choose "Export to GIS" from the File menu, select DXF
      format, press the Properties button, then select the "Contours" pane.

    - Fixed bug in which, when exporting a plot to a GIS, background maps were
      sometimes incompletely exported.

    - The printer options have been moved from the plot options dialog box.
      They are now accessed by choosing "Print options" from the File menu.




Version 4.22

    - Command line option -ExportToDXF added, which exports plot to DXF file.

    - Menu options to display ReadMe and Version History files.

    - Precision of coordinates written to DXF files increased to 9 digits.




Version 4.21

    - Microsoft Intellimouse (mouse wheel) support added.  Roll mouse wheel
      to zoom a plot, drag with middle mouse button to pan a plot.

    - Fixed bug in which the Initial Heading parameter of LINC NMGF section was
      being interpreted incorrectly.




Version 4.2

    - Ability to read and write Noise Model ASCII Grid Format (NMAGF) files.

    - If necessary, extension added to file name when saving.

    - Fixed bug in which program crashed under Windows 95 with the error
      message "A call to MS Windows function CreateDC failed."




Version 4.1

    - Ability to read and display background maps in Digital Line Graph (DLG)
      format.  (Specifically, 1:24000 and 1:100000 maps in DLG level 3 optional
      format)

    - Ability to read, write, and edit map formatting schemes, which control
      how external maps are displayed. (i.e., which features are drawn, which
      colors to use, etc.)

    - The method of navigating a plot with the mouse has been changed.  The old
      'grabbing hand' pan tool has been replaced with a 'center on click' pan
      tool.  Also, with both the pan and zoom tools, the mouse can be used to
      drag a rectangular area that will be expanded to fill the window.

    - In addition to the mouse, can now use the Arrow keys, + and - keys, and
      Home key to scroll, zoom, and goto the home view of a plot.

    - The file "default.nmp" is used as a model for new plots.  This file must
      be in the same directory as NMPlot.exe.

    - Fixed bug in which assertion error "(d0 != 0.0) in file WCCG_Affine-
      Transformation.cpp" occurred when resizing table columns.

    - Fixed bug in which table column could not be resized once it was shrunk
      to its minimum width.

    - Fixed bug in which assertion error in file WC_Map.cpp occurred upon
      application shutdown after using the "Save As" menu option.

    - Fixed bug where program crashes when loading some old Noisemap 6.0-style
      grids that are not in the current directory.

    - Fixed  bug where "Print to Scale" option did not work with some printers.




Version 4.01

    - Ability to load plots and grids from command line added.

    - "Grid/Display plot of this grid" menu option added.

    - Fixed bug reported by Joe Czech, in which spurious error messages,
      stating that function RestoreDC had failed, would be generated while
      printing.




Version 4.0

    - Initial release of 32-bit Microsoft Windows version of NMPlot.  Major
      update.  Product rewritten in C++.  User interface changed to follow
      Microsoft Windows conventions.

      Most significant new capabilities:

          1. Focus of product changed from being noise-model-specific to being
             general-purpose plotting application.

          2. Support for NMBGF 2.0 grid files added.

          3. Ability to view, modify, and save NMBGF grid files added.

          4. Ability to produce color gradient plots added.




Version 3.06

    - Last MS-DOS version of NMPlot.

    - Fixed bug that resulted in immediate "Runtime Error 200" fatal error when
      running NMPlot on machines faster than 200 MHz.




Version 3.05

    - Fixed bug causing improper scaling of contour labels in DXF output.




Version 3.04

    - Fixed bug reported by Jeff Olmstead which resulting in an Invalid Floating
      Point Operation error (error 207) when contouring terrain data in ATAC
      batch mode.




Version 3.03

    - Heuristics added to the contouring algorithm which allow it to produce
      contours of the quality of version 3.01,  while retaining the speed and
      missing data point support of version 3.02.

          1. Heuristic added where nondeterminant triangulation edges are
             oriented parallel to the expected contours.

          2. Heuristic added where the intersection points between contours
             and nondeterminant triangulation edges are not considered when
             drawing contours.

    - HeapLimit and HeapBlock modifications made to decrease out-of-memory
      errors.




Version 3.02

    - Addition of Voronoi polygon contouring algorithm to increase speed for
      large NMBGF files.

    - Addition of support for rotated grids.

    - Addition of support for missing grid points.

    - Fixed bug reported by Jeff Olmstead which resulting in an integer
      overflow in the smoothing algorithm.




Version 3.01

    - Additional code to support special ATAC build of NMPlot added.  This
      allows NMPlot to be run as a slave from the United States Federal
      Aviation Administration's (FAA) Integrated Noise Model (INM).

    - Fixed bugs reported in John D'Aprile's review of NMPlot 3.0
                     for Gregg Fleming.

    - Fixed looped contour bug reported by Paul Gerbi.




Version 3.0

    - Product changed from being NOSIEMAP-specific to being a general-purpose
      noise model post-processor.

    - Added ability to read Noise Model Binary Grid Format 1.0 files.

    - Added ability to handle recursively-defined nested subgrids.

    - Added ability to read and plot LINES data.

    - Added ability to manually label flight tracks, lines, and contours by
      point-and-click.




Version 2.01

    - Fixed bug: Com Port hardware and software flow control was not being
      turned on.  It had been assumed that the Async Plus package turned
      flow control on by default, when in fact it does not.  Code was added
      to the DevOpen.inc file's SetPort subroutine to explicitly enable
      flow control.

    - Fixed bug in code that calculates whether a contour closes or not
      when contour areas are being computed.




Version 2.0

    - Miscellaneous updates




Version 1.2

    - Miscellaneous updates




Version 1.1

    - Miscellaneous updates




Version 1.0

    - Initial release of 16-bit DOS version, written in Turbo Pascal.  This was
      the first public release of an in-house tool that was previously written
      in FORTRAN and ran on a UNIX workstation.
