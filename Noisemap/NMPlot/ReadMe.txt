
This directory contains the NMPlot plotting application.
BaseOps uses NMPlot to create and display contour plots
of its noise calculations.

To learn about the NMPlot application, run NMPlot (i.e.,
from Window Explorer, double-click on NMPlot.exe), then
choose Contents from the Help menu.


