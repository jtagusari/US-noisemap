
<section id='NMPlot_DisplayingGeographicAnnotations_id'
webfilename='displayinggeographicannotations'>
    <title>
        Displaying Geographic Annotations
    </title>
    <para>
        Grids can contain <termtext>geographic annotations</termtext>:
        optional map data stored in the grid. See <iref
        target='NMPlot_GridGeographicAnnotations_id'/>. NMPlot can display
        geographic annotations on a plot.
    </para>
    <section id='NMPlot_TypesOfGeographicAnnotations_id'>
        <title>
            Types of Geographic Annotations
        </title>
        <para>
            There are three types of geographic annotations.
        </para>
        <unorderedlist>
            <listitem>
                <para>
                    <italic>Points</italic> - Point geographic annotations represent
                    point-like features, such as the location of schools and hospitals.
                </para>
            </listitem>
            <listitem>
                <para>
                    <italic>Lines</italic> - Line geographic annotations represent
                    linear features, such as roads and rivers.
                </para>
            </listitem>
            <listitem>
                <para>
                    <italic>Areas</italic> - Area geographic annotations represent
                    two-dimensional features, such a lakes and city boundaries.
                </para>
            </listitem>
        </unorderedlist>
    </section>
    <section
    id='NMPlot_GeographicAnnotationCategoriesAndNames_id'>
        <title>
            Geographic Annotation Categories and Names
        </title>
        <para>
            Every geographic annotation has the following two properties.
        </para>
        <unorderedlist>
            <listitem>
                <para>
                    <italic>Category</italic> - A geographic annotation's category
                    specifies the broad class of objects that the annotation belongs
                    to. Examples of categories are "Schools", "Streets", and "Lakes".
                </para>
            </listitem>
            <listitem>
                <para>
                    <italic>Name</italic> - A geographic annotation's name specifies
                    its proper name: i.e., the name of a specific object. Examples
                    of names are "Baker High School", "Main Street", and "Lake Michigan".
                </para>
            </listitem>
        </unorderedlist>
        <para>
            Use the Summary pane of the Grid Document window to find information
            about the geographic annotations in a grid. The summary report
            lists the types of annotations present in a grid, their categories,
            and their names. See <iref target='NMPlot_ViewingGrids_id'/> for
            more information.
        </para>
    </section>
    <section id='NMPlot_PlotGeographicAnnotationsDisplayRules_id'>
        <title>
            Display Rules
        </title>
        <para>
            The <link target='#NMPlot_PlotOptionsDialogBox_id'>Plot Options
            dialog box</link> has a separate page for each type (point, line,
            or area) of geographic annotation. However, you set the options
            for displaying all three types of annotations in a similar fashion.
            The method for setting display options for line geographic annotations
            will be described. Keep in mind that display options for point
            and area geographic annotations are set in a similar fashion.
        </para>
        <para>
            For reference, here is the page used to specify how line geographic
            annotations should be displayed.
        </para>
        <exhibit>
            <img
            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures.png'/>
        </exhibit>
        <para>
            Note the spreadsheet-like table that occupies most of the page.
            This is referred to as the <termtext>display rule table</termtext>.
            Each row in the table defines a <termtext>display rule</termtext>.
        </para>
        <para>
            The first two columns in the table, labeled Category and Name,
            specify the categories and names that each rule applies to. The
            rest of the columns in the table specify <termtext>display options</termtext>:
            colors, line widths, fonts, etc. The display options specify how
            to draw and label the geographic annotations.
        </para>
        <section>
            <title>
                Searching the Display Rule Table
            </title>
            <para>
                When NMPlot displays a plot, it attempts to draw each of the <link
                target='#NMPlot_PrimaryGrid_id'>primary grid's</link> geographic
                annotations in the following fashion.
            </para>
            <orderedlist>
                <listitem>
                    <para>
                        NMPlot searches the appropriate display rule table (i.e., either
                        the point, line, or area table) for a display rule whose category
                        and name match the geographic annotation's category and name.
                        The table is searched from the top to the bottom.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        If a matching rule is found, the geographic annotation is drawn
                        using the display options associated with that rule.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        If a matching rule is not found, the geographic annotation is
                        not displayed.
                    </para>
                </listitem>
            </orderedlist>
            <para>
                The case of letters (upper verses lower case) is not considered
                when searching for a matching rule. For example, a display rule
                category of "STREET" would match a geographic annotation with
                a category of "street".
            </para>
            <para>
                Spaces are also not considered when searching. For example, a
                display rule category of "PavedRoad" would match a geographic
                annotation with a category of " Paved Road ".
            </para>
        </section>
        <section>
            <title>
                Category and Name Wildcards
            </title>
            <para>
                The Category and Names columns of a display rule table can contain
                <termtext>wildcards</termtext>. These are characters that have
                special meanings. They allow you to match groups of categories
                and names.
            </para>
            <para>
                For example, you can create a rule that will refer to any geographic
                annotation with the word "Road" in its category. Such a rule will
                match annotations with categories of "Primary Road" and "Paved
                Road".
            </para>
            <para>
                You may be familiar with wildcards used by many operating systems,
                which allow you to refer to groups of file names: for example,
                *.txt to refer to all text files. The "*" in this example is a
                wildcard.
            </para>
            <para>
                NMPlot recognizes the following wildcards.
            </para>
            <unorderedlist>
                <listitem>
                    <para>
                        <bold>*</bold> - Matches any sequence of zero or more characters.
                        For example, A*B would match AB, AXB, or AXXB.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <bold>?</bold> - Matches exactly one of any character. For example,
                        A?B would match AXB, but not AB or AXXB.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <bold>[ABC]</bold> - Match any one of the characters A, B, or
                        C. For example, A[XY]B would match AXB or AYB, but not AB, AXXB,
                        or AZB.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <bold>,</bold> - Matches either what is on the left or right of
                        the comma. For example, AA,BB would match AA or BB.
                    </para>
                </listitem>
            </unorderedlist>
            <para>
                Wildcards can be combined in powerful ways. As an example, consider
                a display rule with a Category of "Roads" and a Name of "US10*,IL10?".
                This rule would match roads named US10, US10B, and IL10B, but
                not IL10 or IL10BB.
            </para>
        </section>
        <section>
            <title>
                An Example
            </title>
            <para>
                Consider the following sample display rule table. For simplicity,
                only the Category, Name, and Color columns are shown.
            </para>
            <exhibit>
                <table>
                    <tableheader>
                        <tablecolumnheader horzjust='center'>
                            Category
                        </tablecolumnheader>
                        <tablecolumnheader horzjust='center'>
                            Name
                        </tablecolumnheader>
                        <tablecolumnheader horzjust='center'>
                            Color
                        </tablecolumnheader>
                    </tableheader>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            Peachtree Road
                        </tablecell>
                        <tablecell>
                            Red
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            *Lane
                        </tablecell>
                        <tablecell>
                            Green
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            *
                        </tablecell>
                        <tablecell>
                            Black
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Railroad
                        </tablecell>
                        <tablecell>
                            *
                        </tablecell>
                        <tablecell>
                            Gray
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            River,Creek
                        </tablecell>
                        <tablecell>
                            *
                        </tablecell>
                        <tablecell>
                            Blue
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            *
                        </tablecell>
                        <tablecell>
                            *
                        </tablecell>
                        <tablecell>
                            Brown
                        </tablecell>
                    </tablerow>
                </table>
            </exhibit>
            <para>
                The following table lists the color that would be used to draw
                a variety of geographic annotations, based upon the above rule
                table.
            </para>
            <exhibit>
                <table>
                    <tableheader>
                        <tablecolumnheader horzjust='center'>
                            Annotation Category
                        </tablecolumnheader>
                        <tablecolumnheader horzjust='center'>
                            Annotation Name
                        </tablecolumnheader>
                        <tablecolumnheader horzjust='center'>
                            Color Used To Display Annotation
                        </tablecolumnheader>
                    </tableheader>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            Peachtree Road
                        </tablecell>
                        <tablecell>
                            Red
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            Peachtree Lane
                        </tablecell>
                        <tablecell>
                            Green
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Road
                        </tablecell>
                        <tablecell>
                            Peachtree Street
                        </tablecell>
                        <tablecell>
                            Black
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Railroad
                        </tablecell>
                        <tablecell>
                            L &amp; M RR
                        </tablecell>
                        <tablecell>
                            Gray
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            River
                        </tablecell>
                        <tablecell>
                            Mississippi
                        </tablecell>
                        <tablecell>
                            Blue
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Creek
                        </tablecell>
                        <tablecell>
                            Trout
                        </tablecell>
                        <tablecell>
                            Blue
                        </tablecell>
                    </tablerow>
                    <tablerow>
                        <tablecell>
                            Trail
                        </tablecell>
                        <tablecell>
                            Baker Hill
                        </tablecell>
                        <tablecell>
                            Brown
                        </tablecell>
                    </tablerow>
                </table>
            </exhibit>
            <para>
                Note the last rule in the sample display rule table. The * wildcard
                is used for both the category and name. This rule serves as a
                catch-all, matching any geographic annotation not matched by an
                earlier rule.
            </para>
            <note type='caution'>
                <para>
                    The order of rules is important. Rules are matched starting at
                    the top of the rule table. Place more specific rules above more
                    general rules, as was done in the sample rule table. Consider
                    the last rule in the sample table, which uses the * wildcard for
                    both the category and name. If this rule appeared at the top of
                    the table, the other rules would never be used, since the first
                    rule would match all annotations.
                </para>
            </note>
        </section>
        <section
        id='NMPlot_PlotGeographicAnnotationsDisplayRules_EditingRuleTables_id'>
            <title>
                Editing Rule Tables
            </title>
            <para>
                Rule tables are displayed using a standard spreadsheet control,
                which functions much like a spreadsheet such as Microsoft Excel.
                See <iref target='WCF_SpreadsheetControl_id'/> for more information
                on using spreadsheet controls.
            </para>
            <para>
                Press the Add Row button to add a rule. Press the Remove Row button
                to remove the rule that contains the selected cell. Use the Move
                Row Up and Move Row Down buttons to change the ordering of rules.
            </para>
            <para>
                In the Category column, type the categories that each rule applies
                to. Alternatively, press the Select Categories From List button
                <img src='WCF_ThreeDotsButton_Figure.png'/>. A list of the categories
                in your grid file is displayed, and you can choose from them.
            </para>
            <exhibit>
                <img
                src='NMPlot_PlotOptions_GeographicAnnotations_SelectCategoryDialogBox_ScreenCap.png'/>
            </exhibit>
            <para>
                In the Name column, type the names that each rule applies to.
                Use wildcards if desired. Alternatively, press the Select Names
                From List button <img src='WCF_ThreeDotsButton_Figure.png'/>.
                A list of the names in your grid file that match the rule's categories
                is displayed, and you can choose from them.
            </para>
            <exhibit>
                <img
                src='NMPlot_PlotOptions_GeographicAnnotations_SelectNameDialogBox_ScreenCap.png'/>
            </exhibit>
        </section>
    </section>
    <section id='NMPlot_DisplayingPointGeographicAnnotations_id'>
        <title>
            Displaying Point Geographic Annotations
        </title>
        <para>
            To display point geographic annotations, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Go to the Points page of the <link
                    target='#NMPlot_PlotOptionsDialogBox_id'>Plot Options dialog box</link>.
                </para>
                <exhibit>
                    <img
                    src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    Check the box labeled <italic>Show point geographic annotations</italic>.
                    If this box is not checked, no point annotations will be displayed
                    on your plot.
                </para>
            </listitem>
            <listitem>
                <para>
                    Add display rules to the table, and set the Category and Name
                    columns for each rule. See <iref
                    target='NMPlot_PlotGeographicAnnotationsDisplayRules_id'/> for
                    instructions.
                </para>
            </listitem>
            <listitem>
                <para>
                    For each display rule in the table, set the value of each display
                    option column.
                </para>
                <unorderedlist>
                    <listitem>
                        <para>
                            <italic>Show</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_Show.png'/>
                        </para>
                        <para>
                            Check this box to display point annotations. If this box is not
                            checked, point annotations matched by this display rule will not
                            be displayed.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Symbol</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_Symbol.png'/>
                        </para>
                        <para>
                            Select the symbol used to display point annotations matched by
                            this display rule. Press the Select Symbol button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Select Symbol
                            dialog box, which allows you to browse the available symbols.
                            See <iref target='WCF_SymbolControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Color</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_Color.png'/>
                        </para>
                        <para>
                            Select the color used to display point annotation matched by this
                            display rule. See <iref target='WCF_ColorControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_Label.png'/>
                        </para>
                        <para>
                            Check this box to label each point annotation matched by this
                            display rule. The points' names will be displayed on the plot.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label Font</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_LabelFont.png'/>
                        </para>
                        <para>
                            Select the font used to label point annotations matched by this
                            display rule. Press the Select Font button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Font dialog
                            box, which allows you to browse the available fonts. See <iref
                            target='WCF_FontControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Point Layer and Label Layer Names</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_PointGeographicFeatures_LayerNames.png'/>
                        </para>
                        <para>
                            NMPlot allows you to export a plot to a Geographic Information
                            System (GIS) in a number of common formats. Some GIS formats support
                            the concept of named layers. Type the layer names used for point
                            annotations matched by this display rule. You can specify different
                            layers for the point symbols and the point labels.
                        </para>
                    </listitem>
                </unorderedlist>
            </listitem>
        </orderedlist>
    </section>
    <section id='NMPlot_DisplayingLineGeographicAnnotations_id'>
        <title>
            Displaying Line Geographic Annotations
        </title>
        <para>
            To display line geographic annotations, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Go to the Lines page of the <link
                    target='#NMPlot_PlotOptionsDialogBox_id'>Plot Options dialog box</link>.
                </para>
                <exhibit>
                    <img
                    src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    Check the box labeled <italic>Show line geographic annotations</italic>.
                    If this box is not checked, no line annotations will be displayed
                    on your plot.
                </para>
            </listitem>
            <listitem>
                <para>
                    Add display rules to the table, and set the Category and Name
                    columns for each rule. See <iref
                    target='NMPlot_PlotGeographicAnnotationsDisplayRules_id'/> for
                    instructions.
                </para>
            </listitem>
            <listitem>
                <para>
                    For each display rule in the table, set the value of each display
                    option column.
                </para>
                <unorderedlist>
                    <listitem>
                        <para>
                            <italic>Show</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_Show.png'/>
                        </para>
                        <para>
                            Check this box to display line annotations. If this box is not
                            checked, line annotations matched by this display rule will not
                            be displayed.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Color</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_Color.png'/>
                        </para>
                        <para>
                            Select the color used to draw line annotations matched by this
                            display rule. See <iref target='WCF_ColorControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Width</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_Width.png'/>
                        </para>
                        <para>
                            Type the width of the line, in millimeters, used to draw line
                            annotations matched by this display rule.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Line Pattern</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_LinePattern.png'/>
                        </para>
                        <para>
                            Select the dash pattern (dashed, dotted, solid, etc.) used to
                            draw line annotations matched by this display rule. Press the
                            Select Line Pattern button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Select Line
                            Pattern dialog box, which allows you to browse the available patterns.
                            See <iref target='WCF_LinePatternControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_Label.png'/>
                        </para>
                        <para>
                            Check this box to label each line annotation matched by this display
                            rule. The lines' names will be displayed on the plot.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label Font</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_LabelFont.png'/>
                        </para>
                        <para>
                            Select the font used to label line annotations matched by this
                            display rule. Press the Select Font button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Font dialog
                            box, which allows you to browse the available fonts. See <iref
                            target='WCF_FontControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Line Layer and Label Layer Names</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_LineGeographicFeatures_LayerNames.png'/>
                        </para>
                        <para>
                            NMPlot allows you to export a plot to a Geographic Information
                            System (GIS) in a number of common formats. Some GIS formats support
                            the concept of named layers. Type the layer names used for line
                            annotations matched by this display rule. You can specify different
                            layers for the lines and the line labels.
                        </para>
                    </listitem>
                </unorderedlist>
            </listitem>
        </orderedlist>
    </section>
    <section id='NMPlot_DisplayingAreaGeographicAnnotations_id'>
        <title>
            Displaying Area Geographic Annotations
        </title>
        <para>
            To display area geographic annotations, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Go to the Areas page of the <link
                    target='#NMPlot_PlotOptionsDialogBox_id'>Plot Options dialog box</link>.
                </para>
                <exhibit>
                    <img
                    src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    Check the box labeled <italic>Show area geographic annotations</italic>.
                    If this box is not checked, no area annotations will be displayed
                    on your plot.
                </para>
            </listitem>
            <listitem>
                <para>
                    Add display rules to the table, and set the Category and Name
                    columns for each rule. See <iref
                    target='NMPlot_PlotGeographicAnnotationsDisplayRules_id'/> for
                    instructions.
                </para>
            </listitem>
            <listitem>
                <para>
                    For each display rule in the table, set the value of each display
                    option column.
                </para>
                <unorderedlist>
                    <listitem>
                        <para>
                            <italic>Show</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_Show.png'/>
                        </para>
                        <para>
                            Check this box to display area annotations. If this box is not
                            checked, area annotations matched by this display rule will not
                            be displayed.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Outline</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_Outline.png'/>
                        </para>
                        <para>
                            Check this box to draw the outlines of area annotations matched
                            by this display rule. If you do not display outlines, you should
                            choose to fill the areas (see <italic>Fill</italic> below). Otherwise,
                            the areas will not be visible.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Outline Color</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_OutlineColor.png'/>
                        </para>
                        <para>
                            Select the color used to draw the outlines of area annotations
                            matched by this display rule. See <iref
                            target='WCF_ColorControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Width</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_Width.png'/>
                        </para>
                        <para>
                            Type the width of the line, in millimeters, used to draw the outline
                            of area annotations matched by this display rule.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Outline Pattern</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_OutlinePattern.png'/>
                        </para>
                        <para>
                            Select the dash pattern (dashed, dotted, solid, etc.) used to
                            draw the outline of area annotations matched by this display rule.
                            Press the Select Line Pattern button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Select Line
                            Pattern dialog box, which allows you to browse the available patterns.
                            See <iref target='WCF_LinePatternControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Fill</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_Fill.png'/>
                        </para>
                        <para>
                            You can fill area annotations on your plot with a solid color.
                            Check this box to fill the area annotations matched by this display
                            rule. If you do not fill areas, you should choose to display the
                            area outlines (see <italic>Outline</italic> above). Otherwise,
                            the areas will not be visible.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Fill Color</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_FillColor.png'/>
                        </para>
                        <para>
                            Select the color used to fill area annotations matched by this
                            display rule. See <iref target='WCF_ColorControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Fill Opacity</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_FillOpacity.png'/>
                        </para>
                        <para>
                            Select the opacity of the fill color. The opacity may range from
                            0% (totally transparent) to 100% (totally opaque). An intermediate
                            opacity will tint areas with a translucent color that allows a
                            background map to show through.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_Label.png'/>
                        </para>
                        <para>
                            Check this box to label each area annotation matched by this display
                            rule. The areas' names will be displayed on the plot.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Label Font</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_LabelFont.png'/>
                        </para>
                        <para>
                            Select the font used to label area annotations matched by this
                            display rule. Press the Select Font button <img
                            src='WCF_ThreeDotsButton_Figure.png'/> to display the Font dialog
                            box, which allows you to browse the available fonts. See <iref
                            target='WCF_FontControl_id'/>.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Area Layer and Label Layer Names</italic> - <img
                            src='NMPlot_ScreenCap_PlotOptions_AreaGeographicFeatures_LayerNames.png'/>
                        </para>
                        <para>
                            NMPlot allows you to export a plot to a Geographic Information
                            System (GIS) in a number of common formats. Some GIS formats support
                            the concept of named layers. Type the layer names used for area
                            annotations matched by this display rule. You can specify different
                            layers for the areas and the area labels.
                        </para>
                    </listitem>
                </unorderedlist>
            </listitem>
        </orderedlist>
    </section>
</section>
