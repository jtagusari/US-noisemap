
<section id='NMPlot_CreatingMovies_id'
webfilename='creatingmovies'>
    <title>
        Creating Movies
    </title>
    <para>
        Using &app_name;, you can create a movie from a group of grid
        files. The movie's frames consist of plots of your grid files.
    </para>
    <para>
        As an example, consider the high temperature on January 1 of last
        year, as measured at a number of cities across the United States.
        You could make a grid file containing these measurements, and
        then use &app_name; to create a contour plot of the temperatures,
        displayed over a background map of the United States. This plot
        would show you the distribution of high temperatures on January
        1.
    </para>
    <para>
        Now assume that you have 365 of these grid files, each corresponding
        to a single day of last year. Using &app_name;, you could create
        a movie from these grids. This movie would display the plot of
        high temperatures mentioned in the previous paragraph. Only now,
        the plot would be animated. The contours would move, and you could
        see how the high temperatures varied throughout the year.
    </para>
    <section>
        <title>
            Steps in Creating a Movie
        </title>
        <para>
            To make a movie, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Create grid files from your data. See <iref
                    target='NMPlot_QuickStartGuideToImportingData_id'/>, <iref
                    target='NMPlot_IntroductionToGrids_id'/>, and <iref
                    target='NMGF_Introduction_id'/> for information on creating grid
                    files.
                </para>
                <para>
                    All of the grid files must be located in the same directory, and
                    must have identical file names, with the exception of a number
                    just before the extension at the end of the file name. For example,
                    the following list of grid file names would meet these conditions.
                </para>
                <unorderedlist>
                    <listitem>
                        <para>
                            <filetext>C:\temp\MyData1.grd</filetext>
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <filetext>C:\temp\MyData2.grd</filetext>
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <filetext>C:\temp\MyData3.grd</filetext>
                        </para>
                    </listitem>
                </unorderedlist>
                <para>
                    The movie will display plots of your grid files, ordered by the
                    numbers at the end of the grids' file names. Leading zeros in
                    the file names (MyData<bold>00</bold>9.grd, MyData<bold>0</bold>10.grd,
                    MyData<bold>0</bold>11.grd) are allowed but not required.
                </para>
            </listitem>
            <listitem>
                <para>
                    Create a plot of one of the grids (it does not matter which one).
                    Set all plot options (contour levels, background maps, etc.) as
                    desired. See <iref target='NMPlot_IntroductionToPlots_id'/>.
                </para>
                <note type='tip'>
                    <para>
                        Pay particular attention to the plot's <link
                        target='#WCGISxWCF_HomeView_id'>home view</link>. By default,
                        the home view displays the area covered by the largest contour.
                        This works well for a single plot. However, for movies where the
                        contours move, such a home view may cause the plot's background
                        to also move. This can be distracting.
                    </para>
                    <para>
                        For best results, set the home view to the primary grid and/or
                        the define area polygon. Or, manually specify the home view. See
                        <iref target='WCGISxWCF_HomeView_id'/> for more information.
                    </para>
                </note>
            </listitem>
            <listitem>
                <para>
                    Choose <menutext>Create Movie</menutext> from the <menutext>File</menutext>
                    menu. The Create Movie dialog box appears.
                </para>
                <exhibit>
                    <img src='NMPlot_CreateMovieDialogBox_ScreenCap.png'/>
                </exhibit>
                <para>
                    The Create Movie dialog box is a Multiple Page dialog box. See
                    <iref target='WCF_MultiplePageDialogBoxes_id'/> for more information.
                </para>
                <para>
                    Set the various movie options as desired, and then press the OK
                    button.
                </para>
            </listitem>
            <listitem>
                <para>
                    NMPlot begins creating the movie. Depending on the options you
                    selected, this can take anywhere from a few seconds to a few hours.
                    A dialog box is displayed that shows NMPlot's progress.
                </para>
                <exhibit>
                    <img src='NMPlot_CreatingMovieDialogBox_ScreenCap.png'/>
                </exhibit>
                <para>
                    When the movie is finished, this dialog box disappears. The movie
                    will be in the AVI file whose name you specified on the <link
                    target='#NMPlot_CreateMovie_Options_MovieFile_id'>Movie File</link>
                    page of the Create Movie dialog box. You can display the movie
                    using any third-party multimedia application capable of playing
                    AVI files (for example, Microsoft Media Player). You can also
                    post the movie on the Internet, or load it into a third-party
                    movie editing program for editing.
                </para>
            </listitem>
        </orderedlist>
    </section>
    <section id='NMPlot_CreateMovie_Options_MovieFile_id'>
        <title>
            Movie File
        </title>
        <para>
            Use the Movie File page of the Create Movie dialog box to set
            the name of the file where your movie will be written.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_MovieFile_ScreenCap.png'/>
        </exhibit>
        <para>
            In the box provided, type the name of file where your movie will
            be written. Press the Browse button <img
            src='WCF_ThreeDotsButton_Figure.png'/>, located to the right of
            the text box, to display the Open File dialog box, which allows
            you to browse for the file.
        </para>
        <para>
            Your movie will be written as an AVI file. Typically, AVI files
            have the extension <filetext>.avi</filetext>.
        </para>
    </section>
    <section id='NMPlot_CreateMovie_Options_Size_id'>
        <title>
            Size
        </title>
        <para>
            Use the Size page of the Create Movie dialog box to select the
            size, resolution, and format of the frames in your movie.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_FrameSize_ScreenCap.png'/>
        </exhibit>
        <section
        id='NMPlot_CreateMovie_Options_FrameSize_ColorDepth_id'>
            <title>
                Movie Format
            </title>
            <para>
                Select the format of the frames in your movie. This is a combination
                of the color depth (the maximum number of colors in each frame)
                and the compression method. You have the following choices.
            </para>
            <unorderedlist>
                <listitem>
                    <para>
                        <italic>24-bit Color (True Color), Uncompressed</italic> - Each
                        frame pixel requires 24 bits (3 bytes) of memory. Frames can display
                        over 16,000,000 colors. No compression is used. It is recommended
                        that this format be used if your movie displays a color gradient
                        plot.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>8-bit Color, Optimized Palette, Uncompressed</italic>
                        - Each frame pixel requires 8 bits (1 byte) of memory. Frames
                        can display 256 colors. No compression is used.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>8-bit Color, Optimized Palette, Compressed with RLE</italic>
                        - Each frame pixel requires 8 bits (1 byte) of memory. Frames
                        can display 256 colors. Frames are compressed using Run Length
                        Encoding (RLE). This format will usually result in the smallest
                        movie file.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>8-bit Grayscale, Uncompressed</italic> - Each frame pixel
                        requires 8 bits (1 byte) of memory. Frames can display 256 shades
                        of gray. No compression is used.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>8-bit Grayscale, Compressed with RLE</italic> - Each frame
                        pixel requires 8 bits (1 byte) of memory. Frames can display 256
                        shades of gray. Frames are compressed using Run Length Encoding
                        (RLE).
                    </para>
                </listitem>
            </unorderedlist>
        </section>
        <section>
            <title>
                Movie Frame Size
            </title>
            <para>
                Select the method used to specify the dimensions of the frames
                in your movie (i.e., the width and height of your movie, in pixels).
                You have two choices.
            </para>
            <unorderedlist>
                <listitem>
                    <para>
                        <italic>Movie frames are the same size as the plot on the screen</italic>
                        - Your movie has the same dimensions (in pixels) as the portion
                        of the screen currently used to display the plot that this movie
                        is based upon.
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>Movie frames are</italic> - Type the width and height
                        of your movie, in pixels.
                    </para>
                </listitem>
            </unorderedlist>
        </section>
        <section>
            <title>
                Movie Frame Resolution
            </title>
            <para>
                The resolution of a movie frame measures the size of the pixels.
                It is typically expressed in pixels per inch or pixels per centimeter.
            </para>
            <para>
                As an example, assume that your movie has features that are drawn
                with 1-millimeter-wide lines. If your movie has a resolution of
                100 pixels per centimeter, these lines will be 10 pixels wide.
            </para>
            <para>
                Movies typically have the same resolution as computer monitors:
                i.e., 70 to 90 pixels per inch.
            </para>
            <para>
                Select the method used to specify the resolution. You have two
                choices.
            </para>
            <unorderedlist>
                <listitem>
                    <para>
                        <italic>Movie frames have same resolution as the screen</italic>
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <italic>Movie frames have a resolution of</italic> - Type the
                        resolution, in pixels per inch.
                    </para>
                </listitem>
            </unorderedlist>
        </section>
    </section>
    <section id='NMPlot_CreateMovie_Options_PlaybackSpeed_id'>
        <title>
            Playback Speed
        </title>
        <para>
            Use the Playback Speed page of the Create Movie dialog box to
            set the rate at which your movie should be displayed.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_PlaybackSpeed_ScreenCap.png'/>
        </exhibit>
        <para>
            In the box provided, type the speed at which your movie should
            be played, in frames per second. Typical speeds are between 8
            and 36 frames per second.
        </para>
        <para>
            Divide the total number of frames in your movie (i.e., the number
            of grid files) by the playback speed to determine the total length
            of your movie, in seconds.
        </para>
    </section>
    <section id='NMPlot_CreateMovie_Options_Oversampling_id'>
        <title>
            Oversampling
        </title>
        <para>
            &app_name; can use oversampling to improve the appearance your
            movie. Oversampling reduces jagged edges. In technical terms,
            it antialiases your movie's frames.
        </para>
        <para>
            The amount of oversampling is controlled by the <termtext>oversampling
            factor</termtext>, an integer between one and eight. An oversampling
            factor of one means that oversampling is not performed. Higher
            factors result in smoother, higher-quality movies, but also dramatically
            increase the amount of memory required to create them.
        </para>
        <exhibit>
            <img
            src='WCGISxWCF_ExportingMapsAsBitmapImages_Oversampling_Example_ScreenCap.png'
            standard_colors='true'/>
        </exhibit>
        <para>
            Use the Oversampling page of the Create Movie dialog box to set
            the oversampling factor used when creating your movie.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_Oversampling_ScreenCap.png'/>
        </exhibit>
        <para>
            Type the oversampling factor in the box provided.
        </para>
        <para>
            When using oversampling, you should choose 24-bit true color or
            8-bit grayscale color for your movie's color depth. See <iref
            target='NMPlot_CreateMovie_Options_FrameSize_ColorDepth_id'/>.
        </para>
        <note type='caution'>
            <para>
                Oversampling dramatically increases the amount of memory needed
                to create your movie. The amount of memory required is proportional
                to the square of the oversampling factor.
            </para>
            <para>
                Until you gain familiarity with your computer's capabilities,
                it is recommended that you initially create your movie using a
                low oversampling factor, and then attempt to create it with gradually
                increasing factors. If your computer takes an exceptionally long
                time to create your movie, you may not have enough memory. This
                is especially true if your hard drive light stays on constantly.
                Try reducing the oversampling factor.
            </para>
        </note>
    </section>
    <section id='NMPlot_CreateMovie_Options_LevelOfDetail_id'>
        <title>
            Level of Detail
        </title>
        <para>
            Use the Level of Detail page of the Create Movie dialog box to
            control how much background map detail is displayed in your movie.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_LevelOfDetail_ScreenCap.png'/>
        </exhibit>
        <para>
            Many background maps specify a minimum scale at which various
            features should be displayed. As you zoom in on such a map, additional
            detail appears. The intent is to prevent excessive detail from
            cluttering a plot when it is displayed at a small scale. See <iref
            target='WCGIS_BackgroundMapFormats_id'/>.
        </para>
        <para>
            Use the Level of Detail page to control how &app_name; uses this
            recommended scale information when you create your movie. You
            have four choices for how &app_name; determines how much detail
            to display.
        </para>
        <unorderedlist>
            <listitem>
                <para>
                    <italic>Draw movie frames with a level of detail appropriate for
                    the scale at which the frames are drawn</italic> - Any recommended
                    scale information in the background map is used.
                </para>
            </listitem>
            <listitem>
                <para>
                    <italic>Draw movie frames with the level of detail currently displayed
                    on the screen</italic> - The level of detail is the same as that
                    currently displayed by the plot on your computer's monitor.
                </para>
            </listitem>
            <listitem>
                <para>
                    <italic>Draw movie frames with all details displayed</italic>
                    - Any recommended scale information in the background map is ignored.
                </para>
                <note type='caution'>
                    <para>
                        This may display so much background map detail as to make your
                        movie illegible.
                    </para>
                </note>
            </listitem>
            <listitem>
                <para>
                    <italic>Draw movie frames with a level of detail appropriate for
                    display at a scale of</italic> - Your movie is created with a
                    level of detail appropriate for the scale you specify.
                </para>
            </listitem>
        </unorderedlist>
    </section>
    <section id='NMPlot_CreateMovie_Options_Preview_id'>
        <title>
            Preview
        </title>
        <para>
            Use the Preview page of the Create Movie dialog box to see a summary
            of the options that will be used to create your movie, and to
            preview selected frames.
        </para>
        <exhibit>
            <img src='NMPlot_CreateMovie_Preview_ScreenCap.png'/>
        </exhibit>
        <para>
            The text presents a brief summary of the movie that will be created.
            This summary includes a listing of grid files associated with
            the first and last frames in the movie. To see a full listing
            of the grids files that will be used, press the Show All Grid
            File Names button.
        </para>
        <note type='important'>
            <para>
                Pay particular attention to the size of the AVI movie file that
                will be created. Movies can be very large: ensure that you have
                enough free hard disk space to hold the movie. Also, the AVI file
                must be smaller than 1.96 Gigabytes: a warning will be displayed
                if this limit may be exceeded.
            </para>
            <exhibit>
                <img
                src='NMPlot_CreateMovie_Preview_ExcessiveSizeWarning_ScreenCap.png'/>
            </exhibit>
        </note>
        <para>
            To preview a frame from your movie, type the frame number in the
            box provided, then press the Preview Frame button. The requested
            frame will be displayed. There may be a delay before the frame
            appears.
        </para>
        <note type='tip'>
            <para>
                Movies can take a long time to create. Therefore, you should preview
                a representative sampling of your movie's frames to insure that
                you are satisfied with the options you have selected.
            </para>
        </note>
    </section>
</section>
