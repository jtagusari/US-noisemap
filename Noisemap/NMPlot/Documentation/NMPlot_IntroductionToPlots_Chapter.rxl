
<section id='NMPlot_IntroductionToPlots_id'
webfilename='introductiontoplots'>
    <title>
        Introduction to Plots
    </title>
    <para>
        A <termtext>plot</termtext> is a graphical representation of a
        grid that displays a combination of the following elements.
    </para>
    <unorderedlist>
        <listitem>
            <para>
                A set of contours representing the grid's <link
                target='#NMPlot_ScalarField_Term_id'>scalar field</link>
            </para>
        </listitem>
        <listitem>
            <para>
                Smoothly varying gradients of color representing the grid's scalar
                field
            </para>
        </listitem>
        <listitem>
            <para>
                A map of the location of the grid's data points
            </para>
        </listitem>
        <listitem>
            <para>
                The grid's <link
                target='#NMPlot_GridGeographicAnnotations_id'>geographic annotations</link>
            </para>
        </listitem>
        <listitem>
            <para>
                A background map
            </para>
        </listitem>
    </unorderedlist>
    <para>
        This chapter discusses the basics of creating and configuring
        plots. Later chapters discuss the various elements of a plot in
        detail.
    </para>
    <section id='NMPlot_CreatingNewPlot_id'>
        <title>
            Creating a New Plot
        </title>
        <para>
            To create a new plot, follow these steps.
        </para>
        <orderedlist>
            <listitem id='NMPlot_NewPlotDialogBox_id'>
                <para>
                    Choose <menutext>New</menutext> from the <menutext>File</menutext>
                    menu, then choose <menutext>Plot</menutext>. The New Plot dialog
                    box appears.
                </para>
                <exhibit>
                    <img src='NMPlot_ScreenCap_NewPlot.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    Select the type of plot to create. You have two choices.
                </para>
                <unorderedlist>
                    <listitem>
                        <para>
                            <italic>Create plot of a grid</italic> - This is the most common
                            choice. A contour plot of a <link
                            target='#NMPlot_IntroductionToGrids_id'>grid</link> will be created.
                        </para>
                        <para>
                            Type the name of the grid file that you want the plot to display.
                            Press the Browse button <img
                            src='WCF_ThreeDotsButton_Figure.png'/>, located to the right of
                            the text box, to display the Open File dialog box, which allows
                            you to browse for the file.
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <italic>Create plot showing only a background map</italic> - A
                            plot will be created that shows one or more background map layers.
                            No grid file is required. This is useful for previewing the contents
                            of a map file.
                        </para>
                        <para>
                            Specify the background map layers that you wish to display. See
                            <iref target='WCGIS_MapBackground_Layers_id'/> for more information.
                        </para>
                    </listitem>
                </unorderedlist>
            </listitem>
            <listitem>
                <para>
                    Press OK. A plot document window appears, displaying the new plot.
                </para>
            </listitem>
        </orderedlist>
    </section>
    <section id='NMPlot_PlotOptionsDialogBox_id'>
        <title>
            The Plot Options Dialog Box
        </title>
        <para>
            To display the Plot Options dialog box, you can:
        </para>
        <unorderedlist>
            <listitem>
                <para>
                    Choose <menutext>Options</menutext> from the <menutext>Plot</menutext>
                    menu.
                </para>
            </listitem>
            <listitem>
                <para>
                    Press the Options button <img
                    src='intres:WCF_PropertiesToolbarButtonBitmap'/> on the toolbar.
                </para>
            </listitem>
        </unorderedlist>
        <para>
            Use the Plot Options dialog box to change a plot's appearance.
            All plot options are set from here.
        </para>
        <exhibit>
            <img src='NMPlot_ScreenCap_PlotOptionsDialogBox.png'/>
        </exhibit>
        <para>
            The left portion of the dialog box displays a list of option categories.
            One category in this list is always selected. The right portion
            of the dialog box displays controls that allow you to change the
            options in the selected category.
        </para>
        <para>
            The Plot Options dialog box is a Multiple Page dialog box. See
            <iref target='WCF_MultiplePageDialogBoxes_id'/> for more information.
        </para>
    </section>
    <section id='NMPlot_PrimaryGrid_id'>
        <title>
            The Primary Grid of a Plot
        </title>
        <para>
            The purpose of a plot is to graphically display a grid, which
            is know as the plot's <termtext>primary grid</termtext>.
        </para>
        <note>
            <para>
                In addition to the primary grid, one or more additional grids
                may be displayed as background layers. See <iref
                target='NMPlot_OverlayingGrids_id'/> for more information.
            </para>
        </note>
        <para>
            NMPlot assumes that the primary grid is stored in a file. When
            a plot is created, you are prompted for the name of the primary
            grid file. If you later want to change the primary grid, follow
            these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Display the Primary Grid page of the <link
                    target='#NMPlot_PlotOptionsDialogBox_id'>Plot Options dialog box</link>.
                </para>
                <exhibit>
                    <img src='NMPlot_ScreenCap_PlotOptions_PrimaryGrid.png'/>
                </exhibit>
            </listitem>
            <listitem>
                <para>
                    In the box provided, type the name of the grid file containing
                    the primary grid. Press the Browse button <img
                    src='WCF_ThreeDotsButton_Figure.png'/>, located to the right of
                    the text box, to display the Open File dialog box, which allows
                    you to browse for the file.
                </para>
            </listitem>
        </orderedlist>
        <para>
            To display a plot's primary grid, choose <menutext>Display Primary
            Grid of this Plot</menutext> from the <menutext>Plot</menutext>
            menu. NMPlot will open the primary grid and display a summary
            of it. See <iref target='NMPlot_ViewingGrids_id'/> for more information.
        </para>
    </section>
    <section>
        <title>
            Saving a Plot
        </title>
        <para>
            You can save a plot to a file, referred to as a <termtext>plot
            file</termtext> . All options on the <link
            target='#NMPlot_PlotOptionsDialogBox_id'> Plot Options dialog
            box</link> are saved. Plot files typically have the extension
            <filetext>.nmp</filetext>, which is short for <underline>NM</underline>Plot
            <underline>P</underline>lot.
        </para>
        <para>
            To save a plot, choose either <menutext>Save</menutext> or <menutext>Save
            As</menutext> from the <menutext>File</menutext> menu.
        </para>
    </section>
    <section id='NMPlot_OpeningAPlot_id'>
        <title>
            Opening a Plot
        </title>
        <para>
            To open a plot that has been saved to a file, follow these steps.
        </para>
        <orderedlist>
            <listitem>
                <para>
                    Choose <menutext>Open</menutext> from the <menutext>File</menutext>
                    menu. The standard Windows dialog box for opening files is displayed.
                    Familiarity with the use of this dialog box is assumed.
                </para>
            </listitem>
            <listitem>
                <para>
                    Navigate to the directory where your plot file is located, then
                    open the file. Plot files typically have the extension <filetext>.nmp</filetext>.
                </para>
            </listitem>
            <listitem>
                <para>
                    A Plot Document window appears, displaying the plot.
                </para>
            </listitem>
        </orderedlist>
        <para>
            NMPlot can read all previous versions of plot files.
        </para>
    </section>
</section>
