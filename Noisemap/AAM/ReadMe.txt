
This directory contains the AAM aircraft noise model,
a component of the Noisemap aircraft noise suite.

If you wish to run AAM, you will need to obtain and 
install it separately.  As of January 2021, AAM can
be requested via a web form located at
https://www.volpe.dot.gov/AAM .

Run the AAM installer, and choose to install AAM into 
this directory (i.e., the Noisemap\AAM directory).  After a 
successful installation, the Noisemap\AAM directory will 
have new subdirectories, including Noisemap\AAM\bin (which 
will contain the AAM executable) and Noisemap\AAM\ncfiles 
(which will contain the AAM noise sphere files).

If you've been using previous versions of AAM with BaseOps, 
note the change in where the AAM files are located.  Previously, 
everything was located in the Noisemap\AAM directory.  Now, some 
files are located in subdirectories, as described above.  You can 
delete the old aam.exe and netcdf.dll files from the Noisemap\AAM 
subdirectory, and move your .nc noise sphere files to the 
Noisemap\AAM\ncfiles subdirectory.

After installing AAM, read the file "Default AAM Aircraft.txt" 
for instructions on registering AAM aircraft with BaseOps.


 Files
-------
    
Default AAM Aircraft.txt

    A text file containing information about AAM aircraft that are 
    available for use in BaseOps.  This file contains detailed
    comments describing its purpose and format.  If you add new
    .nc files to AAM, then register them with BaseOps by following 
    the instructions in "Default AAM Aircraft.txt".


bin\aam.exe 

    The AAM executable program.  This program reads an input file 
    describing the flight operations of a number of aircraft, and 
    computes the predicted noise impacts at a large number of points. 
    This file is added by the AAM installer.
    

bin\netcdf.dll

    The NetCDF (Network Common Data Form) dynamic link library.  AAM 
    uses this library to read the .nc noise sphere files.  It 
    must be present in order for AAM to run.  This file is added by the 
    AAM installer.


ncfiles\*.nc

    Noise sphere files.  These files contain the measured noise data 
    for each aircraft that AAM supports.  The files are in NetCDF 
    (Network Common Data Form) format.  This file is added by the AAM 
    installer.

