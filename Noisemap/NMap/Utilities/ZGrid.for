C***********************************************************************
C
C     SUBROUTINE ZGRID
C
C     SETS UP SQUARE GRID FOR CONTOURING , GIVEN ARBITRARILY PLACED
C     DATA POINTS. LAPLACE INTERPOLATION IS USED.
C
C     FORTRAN
C
C     CALL ZGRID(Z,NXSIZE,NYSIZE,NX,NY,X1,Y1,X2,Y2,
C                XP,YP,ZP,N,CAY,NRNG,ZPIJ,KNXT)
C
C     WHERE:
C
C          Z       =  2-D ARRAY OF HGTS TO BE SET UP.
C                     POINTS OUTSIDE REGION TO BE CONTOURED SHOULD BE
C                     INITIALIZED TO 10E35. THE REST SHOULD BE 0.0
C
C          NX,NY    = MAX SUBSCRIPTS OF Z IN X AND Y DIRECTIONS .
C
C          X1,Y1    = COORDINATES OF Z(1,1)
C
C          X2,Y2    = COORDINATES OF Z(NX,NY)
C
C          XP,YP,ZP = ARRAYS GIVING POSITION AND HGT OF EACH DATA POINT.
C
C          N        = SIZE OF ARRAYS XP,YP AND ZP .
C
C          K        = AMOUNT OF SPLINE EQN (BETWEEN 0 AND INF.)
C
C                   =   0. GIVES PURE LAPLACE SOLUTION.
C
C                   = INF. GIVES PURE SPLINE SOLUTION.
C
C          NRNG     = GRID POINTS MORE THAN NRNG GRID SPACES FROM THE
C                     NEAREST DATA POINT ARE SET TO UNDEFINED.
C
C          KNXT     = PASSED WORK ARRAY WHICH MUST BE DIMENSIONED
C                     AT LEAST N POINTS LONG BY THE USER.
C
C          ZPIJ     = PASSED WORK ARRAY WHICH MUST BE DIMENSIONED
C                     AT LEAST N POINTS LONG BY THE USER.
C
C
C     NOTE:
C
C     SMOOTHER RESULTS ARE OBTAINED BY INCLUDING A PORTION OF THE
C     BEAM EQN. IT WAS ADDED TO THE LAPLACE EQN GIVING:
C
C     DELTA2X(Z)+DELTA2Y(Z) - K(DELTA4X(Z)+DELTA4Y(Z)) = 0 .
C
C     IMNEW MUST BE DIMENSIONED AT LEAST NY POINTS LONG.
C
C***********************************************************************
C
C     COPYRIGHT  1984, PLOTWORKS, Inc.,  SAN DIEGO, CA
C
C     COPYRIGHT  1989, PLOTWORKS, Inc.,  SAN DIEGO, CA
C
C***********************************************************************
C
C      HISTORY:
C
C      [1]: 27/JAN/84   CHANGED PASSED PARAMETERS DX,DY TO X2,Y2.
C
C      [2]:  1/APR/84   $DO66 STATEMENT REMOVED.
C
C      [3]:  1/APR/84   COMMENTS CLEANED UP.
C
C      [4]: 30/OCT/84   MADE CORRECTIONS TO ALLOW GRIDS 128 BY 128.
C
C      [5]: 27/JUN/86   ERROR PROCESSING ADDED.
C
C      [6]: 15/Oct/87   Cleaned up error processing.
C
C      [7]: 15/Jan/89   Cleaned up error processing.
C
C      [8]: 17/Sep/89   Made routine handle arrays up to 512x512.
C
C      [9]: 30/Oct/89   Put ITMAX, IMNEW in an include file.
C
C***********************************************************************
C
      SUBROUTINE ZGRID(Z,NXSIZE,NYSIZE,NX,NY,X1,Y1,X2,Y2,
     1    XP,YP,ZP,N,CAY,NRNG,ZPIJ,KNXT)
C
C...  [9]:  Include a work array.
cINCLUDE: '..\BLK\ZGRID.BLK'
      include 'zgrid.blk'

C
C...  [6]: Include error processing block.
cINCLUDE: '..\BLK\MESG.BLK'

C
C...  Dimension statement defining array to hold gridded data.
      DIMENSION Z(NXSIZE,NYSIZE)
C
C...  Arrays to hold the x, y, z coordinates of the random data.
      DIMENSION XP(2),YP(2),ZP(2)
C
C...  Work arrays.  Must be dimensioned at least n long in main prog.
      REAL    ZPIJ(2)
      INTEGER KNXT(2)
C
C...  Long integer work variables declared.
      INTEGER*4 JI1,JI2,JI3
C
C...  [6][7]: Set error to zero initially.
c     CALL SETERR( 0, 0 )
C
C...  [5]: Add error message
      IF( NX .GT. NXSIZE   .OR. NX .LT. 3  .OR.
     -    NY .GT. NYSIZE   .OR. NY .LT. 3  ) THEN
C
C...      Write an error message.
          WRITE( *    , 111 ) NX,NY
C
C...      Display error.
c         CALL SETERR( 1, 15 )
c         CALL MESAGE( iecho, nout, irow2, icol2, ieatr, ilevel,
c    -                 iwarn, ibell, looptm, error, iperr, isize )
C
C...      Then return
          RETURN
C
C...  End of error processing.
      ENDIF
C
C...  Numbers larger than this will not be gridded.
      BIG=.9E35
C
C...
      EPS=.002
C
C...  [1]: Calculate dx, dy from input values.
      DX=(X2-X1)/FLOAT(NX-1)
      DY=(Y2-Y1)/FLOAT(NY-1)
C
C...  Define z-min and z-max to be first value.
      ZMIN=ZP(1)
      ZMAX=ZP(1)
C
C...  Loop through all of the others to determine the min and max z.
      DO 20 K=2,N
      IF(ZP(K)-ZMAX)14,14,12
   12 ZMAX=ZP(K)
   14 IF(ZP(K)-ZMIN)16,20,20
   16 ZMIN=ZP(K)
   20 CONTINUE
C
C...  Find the range that the z values span.
      ZRANGE=ZMAX-ZMIN
C
C...  Make a bias offset of 20*(zmax-zmin) for all values.
      ZBASE=ZRANGE*20.-ZMIN
C
C...  Get minimum world unit distance.
      HRANGE=AMIN1(DX*(NX-1) , DY*(NY-1))
C
C...  This is basically the 2*slope = del(z)/del(dist)
      DERZM=2.*ZRANGE/HRANGE
C
C...  Set pointer array knxt.  Loop through all of the data points.
      DO 60 KK=1,N
C
C...  Index from the last to the first value.
      K=1+N-KK
C
C...  Initialize each pointer to zero initially.
      KNXT(K)=0
C
C...  Compute x index into the z array.
      I= (XP(K)-X1)/DX + 1.5
C
C...  Test to see if the value lies outside the valid area.
      IF(I*(NX+1-I))60,60,35
C
C...  Compute y index into the z array.
   35 J= (YP(K)-Y1)/DY + 1.5
C
C...  Test to see if the value lies outside the valid area.
      IF(J*(NY+1-J))60,60,40
C
C...  Was the value set to 1E35? If yes ignore the point.
   40 IF(Z(I,J)-BIG)45,60,60
C
C...  In all other cases add to the linked list array.  The first time
C...  through the array knxt is set to N+1.  Subsequent passes through
C...  the loop can change this to point at the next valid point for
C...  this grid node.
   45 KNXT(K)=N+1
C
C...  If z(i,j) is less than or equal to zero then this is the first
C...  valid point for this grid location so save this point in z(i,j).
C...  If it is positive however there is already at least one valid
C...  point.  In this case save the next point in knxt at save the
C...  first point in z(i,j).
      IF(Z(I,J))55,55,50
   50 KNXT(K)= Z(I,J)+.5
   55 Z(I,J)=K
   60 CONTINUE
C
C...  Affix each data point zp to its nearby grid point.  Take ave zp if
C...  more than one zp nearby the grid point. Add zbase and complement.
C...  To begin loop through all of the data points.
      DO 80 K=1,N
C
C...  As long as knxt(k) is greater than zero it is a valid point.
      IF(KNXT(K))80,80,65
C
C...  Reset the point counter and sum accumalator to zero.
   65 CONTINUE
      NPT=0
      ZSUM=0.
C
C...  Compute grid node indicies for each node.
      I= (XP(K)-X1)/DX + 1.5
      J =(YP(K)-Y1)/DY + 1.5
C
C...  kk is a local temporary counter to keep track of the
C...  proper pointer.
      KK=K
C
C...  Increment the counter and the accumaltor
   70 NPT=NPT+1
      ZSUM=ZSUM+ ZP(KK)
C
C...  Get the next value in the link-list.  Set the old one negative.
      KNXT(KK)=-KNXT(KK)
      KK = -KNXT(KK)
C
C...  If kk is the last value in the list for that grid node then
C...  recall that kk is N+1 so that we drop down to 75 to compute
C...  average.
      IF(KK-N)70,70,75
C
C...  Compute the average add zbase and compliment.
   75 Z(I,J) = -ZSUM/NPT-ZBASE
C
C...  Bottom of loop.
   80 CONTINUE
C
C...  Initially set each unset grid point to value of nearest known pt.
C...  Any grid point that is still zero is set to -1.E35.
      DO 110 I=1,NX
      DO 110 J=1,NY
      IF(Z(I,J))110,100,110
  100 Z(I,J) = -1.E35
  110 CONTINUE
C
C...  Loop through nrange times.
      DO 199 ITER=1,NRNG
C
C...  Set a counter to zero.
      NNEW=0
C
C...  Loop through all of the grid node points.
      DO 197 I=1,NX
      DO 197 J=1,NY
C
C...  Is z(i,j) set to undefined.. If negative it is undefined
C...  and if it is positive it is defined.
      IF(Z(I,J)+BIG)152,192,192
C
C...  Since it is not defined search outward to nrng looking for a point.
C...  Test to see if j is its minimum value.
  152 IF(J-1)162,162,153
C
C...  NO j is not a minimum.  The first time through jmnew is 0 so we
C...  drop to 154.
  153 IF(JMNEW)154,154,162
C
C...  Determine the z value at the previous j.
  154 ZIJN=ABS(Z(I,J-1))
C
C...  Test to see if the previous z value is undefined.  If it is
C...  undefined ignore the
      IF(ZIJN-BIG)195,162,162
C
C...  YES j is a minimum or
  162 IF(I-1)172,172,163
C
C...
  163 IF(IMNEW(J))164,164,172
C
C...
  164 ZIJN=ABS(Z(I-1,J))
C
C...
      IF(ZIJN-BIG)195,172,172
C
C...
  172 IF(J-NY)173,182,182
  173 ZIJN=ABS(Z(I,J+1))
      IF(ZIJN-BIG)195,182,182
  182 IF(I-NX)183,192,192
  183 ZIJN=ABS(Z(I+1,J))
      IF(ZIJN-BIG)195,192,192
  192 IMNEW(J)=0
      JMNEW=0
      GO TO 197
  195 IMNEW(J)=1
      JMNEW=1
      Z(I,J)=ZIJN
      NNEW=NNEW+1
  197 CONTINUE
      IF(NNEW)200,200,199
  199 CONTINUE
  200 CONTINUE
C
C...  If the magnitude of z(i,j) is greater than or equal to big
C...  set z(i,j) to the absolute value of z(i,j).
      DO 202 I=1,NX
      DO 202 J=1,NY
      ABZ=ABS(Z(I,J))
      IF(ABZ-BIG)202,201,201
  201 Z(I,J)=ABZ
  202 CONTINUE
C
C...  Improve the non-data points by applying point over relaxation
C...  using the laplace-spline equation  (Carred method is used).
      DZRMSP=ZRANGE
      RELAX=1.0
c     write(*,*)'Entering 4000'
      DO 4000 ITER=1,ITMAX
c     write(*,*)'iter =',iter
      DZRMS=0.
      DZMAX=0.
      NPG =0
      DO 2000 I=1,NX
      DO 2000 J=1,NY
      Z00=Z(I,J)
      IF(Z00-BIG)205,2000,2000
  205 IF(Z00)2000,208,208
  208 WGT=0.
      ZSUM=0.
      IM=0
      IF(I-1)570,570,510
  510 ZIM=ABS(Z(I-1,J))
      IF(ZIM-BIG)530,570,570
  530 IM=1
      WGT=WGT+1.
      ZSUM=ZSUM+ZIM
      IF(I-2)570,570,540
  540 ZIMM=ABS(Z(I-2,J))
      IF(ZIMM-BIG)560,570,570
  560 WGT=WGT+CAY
      ZSUM=ZSUM-CAY*(ZIMM-2.*ZIM)
  570 IF(NX-I)700,700,580
  580 ZIP=ABS(Z(I+1,J))
      IF(ZIP-BIG)600,700,700
  600 WGT=WGT+1.
      ZSUM=ZSUM+ZIP
      IF(IM)620,620,610
  610 WGT=WGT+4.*CAY
      ZSUM=ZSUM+2.*CAY*(ZIM+ZIP)
  620 IF(NX-1-I)700,700,630
  630 ZIPP=ABS(Z(I+2,J))
      IF(ZIPP-BIG)650,700,700
  650 WGT=WGT+CAY
      ZSUM=ZSUM-CAY*(ZIPP-2.*ZIP)
  700 CONTINUE
      JM=0
      IF(J-1)1570,1570,1510
 1510 ZJM=ABS(Z(I,J-1))
      IF(ZJM-BIG)1530,1570,1570
 1530 JM=1
      WGT=WGT+1.
      ZSUM=ZSUM+ZJM
      IF(J-2)1570,1570,1540
 1540 ZJMM=ABS(Z(I,J-2))
      IF(ZJMM-BIG)1560,1570,1570
 1560 WGT=WGT+CAY
      ZSUM=ZSUM-CAY*(ZJMM-2.*ZJM)
 1570 IF(NY-J)1700,1700,1580
 1580 ZJP=ABS(Z(I,J+1))
      IF(ZJP-BIG)1600,1700,1700
 1600 WGT=WGT+1.
      ZSUM=ZSUM+ZJP
      IF(JM)1620,1620,1610
 1610 WGT=WGT+4.*CAY
      ZSUM=ZSUM+2.*CAY*(ZJM+ZJP)
 1620 IF(NY-1-J)1700,1700,1630
 1630 ZJPP=ABS(Z(I,J+2))
      IF(ZJPP-BIG)1650,1700,1700
 1650 WGT=WGT+CAY
      ZSUM=ZSUM-CAY*(ZJPP-2.*ZJP)
 1700 CONTINUE
      DZ=ZSUM/WGT-Z00
      NPG=NPG+1
      DZRMS=DZRMS+DZ*DZ
      DZMAX=AMAX1(ABS(DZ),DZMAX)
      Z(I,J)=Z00+DZ*RELAX
 2000 CONTINUE
C
C...  Shift data points zp progressively back to their proper places as
C...  the shape of surface z becomes evident.
      IF(ITER-(ITER/10)*10) 3600,3020,3600
 3020 DO 3400 K=1,N
      KNXT(K) =IABS(KNXT(K))
      IF(KNXT(K))3400,3400,3030
 3030 X=(XP(K)-X1)/DX
      I=X+1.5
      X= X+1.-I
      Y=(YP(K)-Y1)/DY
      J=Y+1.5
      Y=Y+1.-J
      ZPXY = ZP(K)+ZBASE
      Z00 = ABS(Z(I,J))
      ZW=1.E35
      IF(I-1)3120,3120,3110
 3110 ZW = ABS(Z(I-1,J))
 3120 ZE=1.E35
      IF(I-NX)3130,3140,3140
 3130 ZE = ABS(Z(I+1,J))
 3140 IF(ZE-BIG)3160,3150,3150
 3150 IF(ZW-BIG)3180,3170,3170
 3160 IF(ZW-BIG)3200,3190,3190
 3170 ZE=Z00
      ZW=Z00
      GO TO 3200
 3180 ZE=2.*Z00-ZW
      GO TO 3200
 3190 ZW = 2.*Z00-ZE
 3200 ZS=1.E35
      IF(J-1)3220,3220,3210
 3210 ZS = ABS(Z(I,J-1))
 3220 ZN= 1.E35
      IF(J-NY)3230,3240,3240
 3230 ZN = ABS(Z(I,J+1))
 3240 IF(ZN-BIG)3260,3250,3250
 3250 IF(ZS-BIG)3280,3270,3270
 3260 IF(ZS-BIG)3300,3290,3290
 3270 ZN= Z00
      ZS= Z00
      GO TO 3300
 3280 ZN = 2.*Z00-ZS
      GO TO 3300
 3290 ZS = 2.*Z00-ZN
 3300 A=(ZE-ZW)*.5
      B=(ZN-ZS)*.5
      C=(ZE+ZW)*.5-Z00
      D=(ZN+ZS)*.5-Z00
      ZXY=Z00+A*X+B*Y+C*X*X+D*Y*Y
      DELZ=Z00-ZXY
      DELZM=DERZM*(ABS(X)*DX+ABS(Y)*DY)*.80
      IF(DELZ-DELZM)3355,3355,3350
 3350 DELZ=DELZM
 3355 IF(DELZ+DELZM)3360,3365,3365
 3360 DELZ=-DELZM
 3365 ZPIJ(K)=ZPXY+DELZ
 3400 CONTINUE
      DO 3500 K=1,N
      IF(KNXT(K))3500,3500,3410
 3410 NPT=0
      ZSUM = 0.
      I= (XP(K)-X1)/DX + 1.5
      J= (YP(K)-Y1)/DY + 1.5
      KK = K
 3420 NPT = NPT+1
      ZSUM = ZSUM + ZPIJ(KK)
      KNXT(KK)= -KNXT(KK)
      KK = -KNXT(KK)
      IF(KK-N)3420,3420,3430
 3430 Z(I,J) =  -ZSUM/NPT
 3500 CONTINUE
 3600 CONTINUE
C
C...  [5]: Test for zero/zero.
      IF( DZRMS .EQ. 0.  .AND.  NPG .EQ. 0 ) THEN
C
C...       Set the returned array to undefined.
           DO 114 J=1,NY
           DO 113 I=1,NX
              Z(I,J) = 1.E35
  113      CONTINUE
  114      CONTINUE
C
C...       Write out an error message.
           WRITE( *    , 222 ) DZRMS, NPG
C
C...       [6]: Display error.
c          CALL SETERR( 1, 16 )
c          CALL MESAGE( iecho, nout, irow2, icol2, ieatr, ilevel,
c    -                  iwarn, ibell, looptm, error, iperr, isize )
C
C...       Return on error.
           RETURN
C
C...  End of error processing.
      ENDIF
C
C...  Test for convergence.
      DZRMS=SQRT(DZRMS/NPG)
      ROOT =DZRMS/DZRMSP
      DZRMSP=DZRMS
      DZMAXF=DZMAX/ZRANGE
      IF(ITER-(ITER/10)*10-2)3715,3710,3715
 3710 DZRMS8 = DZRMS
 3715 IF(ITER-(ITER/10)*10)4000,3720,4000
 3720 ROOT = SQRT(SQRT(SQRT(DZRMS/DZRMS8)))
      IF(ROOT-.9999)3730,4000,4000
 3730 IF(DZMAXF/(1.-ROOT)-EPS)4010,4010,3740
C
C...  Improve the relaxation factor.
 3740 CONTINUE
      AAAA=(ITER-20)+.5
      JI1=AAAA
      AAAA=(ITER-40)+.5
      JI2=AAAA
      AAAA=(ITER-60)+.5
      JI3=AAAA
      IF(JI1*JI2*JI3)4000,3750,4000
 3750 IF(RELAX-1.-ROOT)3760,4000,4000
 3760 TPY =(ROOT+RELAX-1.)/RELAX
      ROOTGS = TPY*TPY/ROOT
      RELAXN= 2./(1.+SQRT(1.-ROOTGS))
      IF(ITER-60)3780,3785,3780
 3780 RELAXN= RELAXN-.25*(2.-RELAXN)
 3785 RELAX = AMAX1(RELAX,RELAXN)
 4000 CONTINUE
 4010 CONTINUE
C
C...  Remove zbase from array z and return.
      DO 4500 I=1,NX
      DO 4500 J=1,NY
      IF(Z(I,J)-BIG)4400,4500,4500
 4400 Z(I,J)=ABS(Z(I,J))-ZBASE
 4500 CONTINUE
C
C...  Error formats.
  111 FORMAT(' PLOT88 Error #15 - Invalid "NX" or "NY" value = ',
     -I5,1X,I5)
  222 FORMAT(' PLOT88 Error #16 - ZGRID Convergence error. Status= ',
     -E8.2,', ',I5)
C
C...  End of subroutine.
      RETURN
      END
