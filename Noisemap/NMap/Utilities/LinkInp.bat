echo off
rem file : LINKINT.BAT   (c) Interactive Software Services 1989-1993
rem Modify according to your own requirements !
if a%intlib%  == a set intlib=c:\int\lib
if a%inttemp% == a set inttemp=inttemp$.$$$
if not a%1    == a goto FILCHECK
cls
echo   . 
echo   様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
echo    LINKINT links up  to  9 object files, using the 32-bit Microsoft portable
echo    linker and the INTERACTER library (Phar Lap bindings). After a successful
echo    link your program is executed. Programs must be run from the standard DOS
echo    prompt (i.e. not from Windows 3.x).
echo   . 
echo    Syntax : LINKINT PROG1 [PROG2 ... PROG9] (link PROG1.OBJ, PROG2.OBJ...)
echo      or     LINKINT                     (to display this help information)
echo   . 
echo    e.g.     LINKINT MAINPROG SUBS
echo   様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
echo    The INTERACTER libraries are expected to be in %intlib%
echo   . 
echo    This can both be changed by setting the environment variable INTLIB
echo    in your AUTOEXEC.BAT file.  The directory containing  the Microsoft
echo    linker is assumed to lie in the current PATH.
echo   様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
goto EXIT
:FILCHECK
if exist %1.obj goto DOLINK
echo . 
echo   Unknown file %1.obj
echo . 
goto EXIT
:DOLINK
echo %1.obj                 >  %inttemp%
if not a%2 == a echo %2.obj >> %inttemp%
if not a%3 == a echo %3.obj >> %inttemp%
if not a%4 == a echo %4.obj >> %inttemp%
if not a%5 == a echo %5.obj >> %inttemp%
if not a%6 == a echo %6.obj >> %inttemp%
if not a%7 == a echo %7.obj >> %inttemp%
if not a%8 == a echo %8.obj >> %inttemp%
if not a%9 == a echo %9.obj >> %inttemp%
echo %intlib%\shortmfp.lib  >> %inttemp%
echo %intlib%\intpcmfp.lib  >> %inttemp%
echo %intlib%\intbinmd.lib  >> %inttemp%
fl32 @%inttemp%
if errorlevel 1     goto TIDY
del %inttemp%
echo . 
echo About to run %1 (Press CTRL/C to return to DOS)
pause
%1
goto EXIT
:TIDY
del %inttemp%
:EXIT
