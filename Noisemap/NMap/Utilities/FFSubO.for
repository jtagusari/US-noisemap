      subroutine dlgheado(filnam,numlin,a2,catnam,izone)
c
c This routine reads past the header, node and area records of an optional
c format DLG file.
c
      character*80 inline
      character*40 name
      character*20 catnam,filnam
      character*10 year
      character*8 edgflg
      character*4 maxcon,maxbat,mincon,minbat,flags
      character*3 sectn
      character*1 qualif,rtype
      character*2 cornam(4)
      dimension numels(6),trans(4),utmcrn(4,2),geocrn(4,2)
      dimension nodflg(2),iarflg(3)
      double precision proj(15),resol,a2,xsw,xse,ysw,yse
      common /plugh/resol
c
c     write(6,*)'In dlgheado: about to open '//filnam
      open(unit=1,file=filnam)
c     write(6,*)'opened'
c     close(6)
c
c------------------------------------------------
c
c Record 1.  Banner
      read(1,'(a)')inline
c Record 2.  Title, date, etc.
      read(1,1001)name,year,qualif,iscale,sectn
 1001 format(a40,1x,a10,a1,i8,3x,a3)
c
c Record 3.  contour spacings and flags
c
      read(1,1002)maxcon,maxbat,mincon,minbat,flags,edgflg
 1002 format(41x,a4,1x,a4,1x,a4,1x,a4,a4,a8)
c
c Record 4 -
c     DLG level code (usually 3 = DLG 3)
c     Planimetric ref. code (usually 1 = UTM)
c     UTM zone
c     Ground units (usually 2 = meters)
c     Resolution (expect 2.54 = 2.54 meters/.001 inch)
c     Number of file-to-map trans params (expect 4)
c     # misc records (expect 0)
c     Number of control points (expect 4)
c     # categories in file (expect 1)
c
      read(1,1003)idlg,itype,izone,iunits,resol,nparms,numac,nctrl,iq
 1003 format(4i6,d18.11,4i6)
c
c Records 5-9
c
c     write(6,*)'About to read records 5-9'
      read(1,1004)proj
 1004 format(3d24.15)
c
c Record 10. Projection transformation parameters; should be 1,0,0,0
c
      read(1,1005)trans
 1005 format(4d18.11)
c
c Records 1-n.  Control point label and points
c
      read(1,1006)(cornam(i),(geocrn(i,k),k=1,2),
     1                       (utmcrn(i,k),k=1,2),i=1,4)
 1006 format(a2,4x,2f12.6,6x,2f12.2)
c
c Make an effective "a2" as minus the SW->SE edge slope, times resolution
c
      do 5, i = 1,4
      if(cornam(i).eq.'SW') then
           xsw = utmcrn(i,1)
           ysw = utmcrn(i,2)
        endif
      if(cornam(i).eq.'SE') then
           xse = utmcrn(i,1)
           yse = utmcrn(i,2)
        endif
 5    continue
c
c 8/14/98  Original formula gives tan(tilt angle).  Really needed sine.
c Tilt angle is the clockwise rotation of the UTM grid relative to the
c geographic coordinates.
c     a2 = resol*(ysw-yse)/(xse-xsw)
      a2 = resol*(ysw-yse)/dsqrt((xse-xsw)**2 + (ysw-yse)**2)
c
c Record 1-q.  Numbers of each type of element.  NUMELS has two values
c for each element: the first is the highest ID number, and the second
c is the actual number (usually the same).  So 2,4,6 correspond to nodes,
c areas, and lines.  Interspersed are:
c     IFMT - normally zero, indicating default formatting of major-minor
c            attribute pairs
c     NODFLG - 2 element flag indicating presence of node-area list and
c              node-line list.  Normally 01.
c     IARFLG - 3 element flag indicating presence of area-node list, area-line
c              list, and area coordinates.  Normally 010.
c     LINCRD - flag indicating presence of line coordinate list.  Normally 1.
c
      read(1,1009)catnam,ifmt,numels(1),numels(2),nodflg,numels(3),
     1      numels(4),iarflg,numels(5),numels(6),lincrd
 1009 format(a20,i4,2i6,1x,2i1,1x,2i6,1x,3i1,2i6,3x,i1)
c
      numlin = numels(6)
c
c------------------------------------------
c
c Header records have all been read.  Read past nodes.
c
c     write(6,*)'Before 10 loop'
      do 10, i = 1,numels(2)
c     write(6,*)'i=',i
      read(1,1010)rtype,id,x,y,nars,nlins,npts,numatt,ntext,nislnd
 1010 format(a1,i5,2f12.2,6i6)
      if(rtype.ne.'N' .or. id.ne.i) then
c          call igrquit
c          call iscreeninit('c')
           write(*,*)'Got '//rtype//' instead of N'
           write(*,*)'I, ID =',i,id
           stop ' '
        endif
      if(nars.gt.0) read(1,1011)(iarea,j=1,nars)
      if(nlins.gt.0) read(1,1011)(line,j=1,nlins)
      if(numatt.gt.0) read(1,1011)(iattr,iattr2,j=1,numatt)
 1011 format(12i6)
 10   continue
c     write(6,*)'out of 10 loop'
c     write(6,*)'numels(4) =',numels(4)
c     write(6,*)
c
c Read past areas
c
      do 20, i = 1,numels(4)
c     write(6,*)' 20 loop: i =',i
      read(1,1010)rtype,id,x,y,nnds,nlins,npts,numatt,ntext,nislnd
c     write(6,*)'just read line'
      if(rtype.ne.'A') write(6,*)'Got ', rtype, ' instead of A'
c     write(6,*)'one'
c     write(6,'(1x,a1,1x)')rtype
c     if(id.ne.i) write(6,*)'I, ID =',i,id
c     write(6,*)'two'
      if(nnds.gt.0) read(1,1011)(iarea,j=1,nnds)
c     write(6,*)'three'
c     if(npts.gt.0) read(1,1011)(ipnts,j=1,npts)
      if(nlins.gt.0) read(1,1011)(line,j=1,nlins)
c     write(6,*)'four'
      if(numatt.gt.0) read(1,1011)(iattr,iattr2,j=1,numatt)
c     write(6,*)'five'
 20   continue
c
      return
      end
c
      subroutine dlglino(n,xmin,xmax,ymin,ymax,catnam,x,y,nprs,inside,
     1                   izone,izon2)
c
c Routine to extract the next data line from the current Optional format
c DLG file.  Arguments are a subset of DLGLINE (standard format) arguments.
c Only UTM coordinates are returned.
c
      save
      character*2 type
      character*20 catnam
      double precision north,east,seclat,seclon,resol
      character*1 flag1,flag2
      logical conflg
c
      dimension x(3000),y(3000),iattr(2,20)
      common /plugh/resol
c
      data flag1,idlat,imlat,seclat,flag2,idlon,imlon,seclon,ispher
     +    /' ',   0,    0,    0.0,  ' ',   0,    0,    0.0,   0/
c
      inside = 0
c
      read(1,1012)type,intid,node1,node2,iarea1,iarea2,nprs,natt,ktext
 1012 format(a1,i5,4i6,12x,3i6)
      if(intid.ne.n .or. type.ne.'L') then
           write(*,*)'Got '//type//' instead of L'
           write(*,*)'Line mismatch: n,intid =',n,intid
           stop' '
        endif
      if(nprs.gt.3000)write(*,*)'n,nprs =',n,nprs
c     write(16,*)'n,nprs =',n,nprs
c
      if(nprs.gt.0) read(1,1013)(x(i),y(i),i=1,nprs)
 1013 format(3(2f12.2))
c
      if(natt.gt.20) then
c          call igrquit
c          call iscreeninit('c')
           write(*,*)'natt=',natt,'  at line # ',intid
           stop 'Fatal error: exceeds program dimensions'
        endif
c
      if(natt.gt.0) then
           read(1,1011)(iattr(1,na),iattr(2,na),na=1,natt)
 1011      format(12i6)
c          write(16,*)'Attributes:'
c          write(16,1011)(iattr(1,na),iattr(2,na),na=1,natt)
        endif
c
c Skip entities with no attributes
c 3/25/96 - plot all PL lines, regardless of attributes
c     if(natt.eq.0 .and. catnam(1:5).ne.'PUBLI') go to 60
c 3/25/96 - skip any line that bounds area 1
      if(iarea1.eq.1 .or. iarea2.eq.1) go to 60
c
c Road layer: skip certain minor roads.  This is a hard-wired action
c for a "p" in position 20 of CATNAM.
c
      if(catnam(1:5).eq.'ROADS'
     +  .and. catnam(20:20).eq.'p'
     +  .and. iattr(1,1).eq.170) then
           if(iattr(2,1).eq.210) go to 60
        endif
c
c Opposite deal: keep only the minor roads.
      if(catnam(1:5).eq.'ROADS'
     +  .and. catnam(20:20).eq.'l'
     +  .and. iattr(1,1).eq.170) then
           if(iattr(2,1).ne.210) go to 60
        endif
c
c
c Special deal: keep stuff associated with interstates: that means attr#2
c is 172, but there are others
      if(catnam(1:5).eq.'ROADS'
     +  .and. catnam(20:20).eq.'i'
     +  .and. iattr(1,1).eq.170) then
           if(iattr(1,2).ne.172) go to 60
           if(natt.eq.2) go to 60
        endif
c
c Special deal: keep primary interstate chunks: that means attr#2
c is 172, and there are no others
      if(catnam(1:5).eq.'ROADS'
     +  .and. catnam(20:20).eq.'I'
     +  .and. iattr(1,1).eq.170) then
           if(iattr(1,2).ne.172) go to 60
           if(natt.gt.2) go to 60
        endif
c
c
c Misc. transp. layer: plot only data with attribute 403, airports
      if(catnam(1:5).eq.'PIPE '
     +  .and. iattr(1,1).eq.190) then
           if(iattr(2,1).ne.403) go to 60
        endif
c
c Hypsography layer:
c Look for data with a primary attribute of 20 and a secondary attibute of 200.
c This is elevation data.
      if(catnam(1:5).eq.'HYPSO') then
           if(natt.eq.0) go to 60
           conflg = .false.
           do 30, i = 1,natt
           if(iattr(1,i).eq.20 .and. iattr(2,i).eq.200) conflg = .true.
 30        continue
           if(.not.conflg) go to 60
c
c Contour value is secondary value of pair with primary attribute 24.  If
c there is no such attribute pair, skip it.
c
           conflg = .false.
           do 40, i = 1, natt
           if(iattr(1,i).eq.24) then
                ielev = iattr(2,i)
                elevat = float(ielev)
                conflg = .true.
             endif
 40        continue
           if(.not.conflg) go to 60
c
        endif
c
c Process the line: convert zone if necessary, then count the number of
c inside points.
c
      do 50, i = 1,nprs
c
c If the UTM zone is not the base, convert
      if(izone.ne.izon2) then
           iz = izone
           iz2 = izon2
           east  = x(i)
           north = y(i)
           idlat = 0
           imlat = 0
           seclat = 0.
           idlon = 0
           imlon = 0
           seclon = 0.
           call utmsub(flag1,idlat,imlat,seclat,flag2,idlon,imlon,
     +                 seclon,north,east,iz,ispher,iz2)
c          write(6,*)x(i),y(i),east,north
           x(i) = east
           y(i) = north
        endif
      if((x(i).gt.xmin .and. x(i).lt.xmax) .and.
     1   (y(i).gt.ymin .and. y(i).lt.ymax)) then
           inside = inside+1
           if(catnam(1:5).eq.'HYPSO')
     +            write(16,'(3f10.0)')x(i),y(i),elevat
        endif
 50   continue
c
 60   continue
c
      return
      end
