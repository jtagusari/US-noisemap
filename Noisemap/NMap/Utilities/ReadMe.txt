This archive contains the following programs:

MAKECTL.EXE     Creates the control file listing DLG files to process for
                elevation data
DLGHGT.EXE      Extracts elevtions from the files listed in the control
                file
GRIDIT.EXE      Generates an ELV file
ADDCR80.EXE     Inserts line breaks in USGS DLG files

Sources for these programs are included.  All are built with Microsoft
Fortran Powerstation 1.0a.  BAT files to link MAKECTL, DLGHGT and GRIDIT
are included.  ADDCR80 is in a single module, so no special link list is
required.

MAKECTL requires the Interacter library, Version 3.15.  This is a
commercial library, and is not included.  File LINKINP.BAT is a procedure
to link with that library, assuming normal installation.
