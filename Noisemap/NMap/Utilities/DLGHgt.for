      program dlghgt
c
c This program extracts the height data from a list of DLG files.
c The list of DLG files is created by "MAKECTL", and is the same as the
c control ("CTL") file used by FFMGR.
c
      parameter (maxfiles = 200)
      double precision a2,resol
      dimension x(3000),y(3000)
      character*20 catnam
      character*12 files(maxfiles),infile
      character*12 dumpfl
      character*2 laytyp
      character*3 czone
      common /plugh/resol
c
c Open the elevation dump file
      if(nargs().ge.3) then
           call getarg(2,dumpfl,ierr)
           l = ierr
           open(unit=16,file=dumpfl(1:l))
         else
           write(*,*)'Correct usage is:'
           write(*,*)
           write(*,*)'dlghgt contfile elevfile'
           write(*,*)
           write(*,*)'where contfile = list of DLG files and limits'
           write(*,*)
           write(*,*)'      elevfile = output XYZ file'
           stop ' '
        endif
c
c Open the control file, as specified on the command line, then read it.
c
      call getarg(1,infile,ierr)
      open(unit=2,file=infile)
      read(2,2001)izon2
 2001 format(3x,i7)
c
      read(2,2002)xmin,xmax,ymin,ymax
 2002 format(2f10.0)
c
c The remaining lines of the file contain the DLG file name, a "special
c action" code (can be blank) and a color.  The special action code and
c color are not used by this program, and so are not read.
c
c Only hypsography (HP) files are used, so skip any non-hypso.
c
      nfiles = 0
 3    continue
      nfiles = nfiles+1
      read(2,'(a12)',end=4)files(nfiles)
      laytyp = files(nfiles)(4:5)
      if(laytyp.ne.'hp' .and. laytyp.ne.'HP') nfiles = nfiles - 1
      go to 3
 4    continue
      nfiles = nfiles-1
      close(2)
c
c Read the selected DLG files
c
      npnts = 0
      do 90, layer = 1,nfiles
c
c
      call dlgheado(files(layer),numlin,a2,catnam,izone)
c
c For the first file, compute the rotation angle of UTM coordinates re
c lat/long.  Note which zone this file is in.
c
      if(layer.eq.1) then
           utmang = asin(sngl(a2)/resol)
           izang = izone
        endif
c
c IZON2 is the UTM zone to which we want to convert all data if it's not
c there to begin with.  IZON2 was read from the first line of the CTL file.
c If it was zero (or blank, which is read as zero), then the zone of the
c first file read determines that zone.
c
      if(layer.eq.1 .and. izon2.eq.0) izon2 = izone
c
c Write the rotation angle and base zone to the HP dump file
c
      if(layer.eq.1) write(16,'(f15.9,i5,i10)')utmang,izon2,0
c
c We really want UTMANG to be for a file in IZON2.  If IZANG is not
c equal to IZON2, then update UTMANG when a file in IZON2 comes up.
c The output file will be updaed at the end, when the first line is
c rewritten to include the number of points.
c
      if(izang.ne.izon2 .and. izone.eq.izon2) then
           utmang = asin(sngl(a2)/resol)
           izang = izone
        endif
c
c Read through the line data.  The 60 loop is through the lines.  The "inside"
C points are written to the file in the subroutine.
c
      do 60, n = 1,numlin
      call dlglino(n,xmin,xmax,ymin,ymax,catnam,x,y,
     +nprs,inside,izone,izon2)
      npts = npts + inside
 60   continue
c
      close(1)
 90   continue
c
      close(16)
c
c Re-open the file as direct access so as to write the number of points
c (known at the end) at the top.
c
      open(unit=16,file=dumpfl(1:l),form='formatted',
     +      access='direct',recl=30)
      write(16,'(f15.9,i5,i10)',rec=1)utmang,izon2,npts
      close(16)
c
c There is a problem if none of the files was actually in the specified
c UTM zone.
      if(izang.ne.izon2) then
           write(czone,'(i3)')izon2
           write(*,*)'Warning!  None of the DLG files is in the specifie
     +d UTM zone'//czone
           write(*,*)'The rotation angle in the output file is not valid
     +'
        endif
c
      end
