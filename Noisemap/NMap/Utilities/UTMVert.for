      subroutine utmvert(flong,flat,x0,y0,izone)
      double precision north,east,seclat,seclon
      double precision flong,flat,flonn,flatt,fmnlon,fmnlat
      character*1 flag1,flag2
c
c Prepare inputs for UTM conversion
c
      flag1 = '+'
      flatt = dabs(flat)
      if(flat.lt.0.d0) then
           flag1 = '-'
        endif
      latdeg = flatt
      fmnlat = 60.*(flatt - dble(float(latdeg)))
      minlat = fmnlat
      seclat = 60.*(fmnlat - dble(float(minlat)))
c
      flag2 = '+'
      flonn = dabs(flong)
      if(flong.gt.0.d0) then
           flag2 = '-'
        endif
      londeg = flonn
      fmnlon = 60.*(flonn - dble(float(londeg)))
      minlon = fmnlon
      seclon = 60.*(fmnlon - dble(float(minlon)))
c
      ispher = 0
      east = 0.
      north = 0.
      iz2 = izone
      iz = izone
      call utmsub(flag1,latdeg,minlat,seclat,flag2,
     +     londeg,minlon,seclon,north,east,iz,ispher,iz2)
      x0 = east
      y0 = north
      return
      end
