      subroutine corneo(filnam,utmcrn,izone)
c
c This routine reads DLG header records and returns the UTM corner coord-
c inates and zone, for an optional format file.
c
      character*80 inline
      character*40 name
      character*12 filnam
      character*10 year
      character*8 edgflg
      character*4 maxcon,maxbat,mincon,minbat,flags
      character*3 sectn
      character*1 qualif
      character*2 cornam(4)
      dimension trans(4),utmcrn(4,2),geocrn(4,2)
      double precision proj(15),resol
c
      open(unit=1,file=filnam)
c
c------------------------------------------------
c
c Record 1.  Banner
      read(1,'(a)')inline
c Record 2.  Title, date, etc.
      read(1,1001)name,year,qualif,iscale,sectn
 1001 format(a40,1x,a10,a1,i8,3x,a3)
c
c Record 3.  contour spacings and flags
c
      read(1,1002)maxcon,maxbat,mincon,minbat,flags,edgflg
 1002 format(41x,a4,1x,a4,1x,a4,1x,a4,a4,a8)
c
c Record 4 -
c     DLG level code (usually 3 = DLG 3)
c     Planimetric ref. code (usually 1 = UTM)
c     UTM zone
c     Ground units (usually 2 = meters)
c     Resolution (expect 2.54 = 2.54 meters/.001 inch)
c     Number of file-to-map trans params (expect 4)
c     # misc records (expect 0)
c     Number of control points (expect 4)
c     # categories in file (expect 1)
c
      read(1,1003)idlg,itype,izone,iunits,resol,nparms,numac,nctrl,iq
 1003 format(4i6,d18.11,4i6)
c
c Records 5-9
c
      read(1,1004)proj
 1004 format(3d24.15)
c
c Record 10. Projection transformation parameters; should be 1,0,0,0
c
      read(1,1005)trans
 1005 format(4d18.11)
c
c Records 1-n.  Control point label and points
c
      read(1,1006)(cornam(i),(geocrn(i,k),k=1,2),
     1                       (utmcrn(i,k),k=1,2),i=1,4)
 1006 format(a2,4x,2f12.6,6x,2f12.2)
c
      return
      end
