      program pick
c
c This program gets a list of DLG-type files and lets the user select
c the ones he/she wants.
c
      parameter (maxfil=200)
      character*80 line1
      character*12 files(maxfil),ctlfil
      character*13 pfiles(maxfil)
      dimension icolr(7),layer(maxfil)
      dimension utmcor(4,2)
      character*2 type(7)
      character*1 code(7)
      logical optional
      data type /'HP','PL','HY','MT','RD','RR','BD'/
      data code /' ',' ',' ',' ','p',' ',' '/
c     data icolr/10,3,9,5,8,2,6/
      data icolr/10,3,1,5,7,2,6/
c
      call iscreeninit(' ')
      last = 1
      ixpos = 0
      iypos = 10
      nvisib = 10
c
c Slight touch of class: double line frames
      call iframetype(2)
c
c Enable single popup mode
      call inpopup('on')
c
c Write directory message at bottom of the screen
      call ioutmessage('Space or mouse to select/deselect files; Enter w
     1hen done')
c
c Get the current directory's files, putting them in FILES after the
c directories
      nfils = 0
      do 20, i =1,7
      nfilf = 200-nfils
      call iosdirentrytype('f')
      call iosdirlist(' ','???'//type(i)//'*',files(nfils+1),nfilf)
c
c Sort this group, then copy it into the pfile array, which has room
c for a "select" character.  Also mark the current type in the LAYER
c array
      if(nfilf.gt.0) then
           call ssort(files(nfils+1),nfilf)
           do 10, j=nfils+1,nfils+nfilf
           pfiles(j)=' '//files(j)
           layer(j) = i
 10        continue
           nfils = nfils+nfilf
        endif
 20   continue
c
c Present the file list in a scrolling menu.
c
c     call inmultiple(char(175))
      call inmultiple(char(254))
      call incontrolkey(34,32)
      call incontrolkey(35,0)

 50   continue
      iwant = imenuscroll(pfiles,nfils,ixpos,iypos,'Select Files:',
     1               nvisib,2,last)
c
c Pull out the selected ones
c
      kpick = 0
      do 60, i = 1,nfils
      if(pfiles(i)(1:1).eq.' ') go to 60
      kpick = kpick+1
      files(kpick) = pfiles(i)(2:13)
      layer(kpick) = layer(i)
 60   continue
c
      call iclearscreen
c
      write(*,'(i5,1x,a)')kpick,'files selected'
      if(kpick.eq.0) stop 'Nuthin for me to do!  Bye.'
      write(*,*)'Enter name of control file (including .ctl) to write:'
      read(*,'(a)') ctlfil
      write(*,*)'Enter UTM zone to place data; 0 to align with zone of f
     +irst file:'
      read(*,*)izon2
      open(unit=2,file=ctlfil)
      xmin = 1.e8
      xmax = 0.
      ymin = 1.e8
      ymax = 0.
c
      do 70, i=1,kpick
      open(unit=1,file=files(i))
      read(1,'(a)')line1
      close(1)
      optional = index(line1,'USGS') .gt. 0
c
      if(optional) then
           call corneo(files(i),utmcor,izone)
         else
           call corner(files(i),utmcor,izone)
        endif
c
      if(i.eq.1 .and. izon2.eq.0) izon2 = izone
      if(izon2.ne.izone) call recorn(utmcor,izon2,izone)
c
      xmax = amax1(xmax,utmcor(1,1),utmcor(2,1),utmcor(3,1),utmcor(4,1))
      xmin = amin1(xmin,utmcor(1,1),utmcor(2,1),utmcor(3,1),utmcor(4,1))
      ymax = amax1(ymax,utmcor(1,2),utmcor(2,2),utmcor(3,2),utmcor(4,2))
      ymin = amin1(ymin,utmcor(1,2),utmcor(2,2),utmcor(3,2),utmcor(4,2))
c
 70   continue
      write(2,'(a3,i7)')'utm',izon2
      write(2,'(2f10.0)')xmin,xmax,ymin,ymax
      write(2,'(a12,8x,a1,28x,i2)')(files(i),code(layer(i))
     1   ,icolr(layer(i)),i=1,kpick)
      close(2)
c
c See what the exit key was
      ipress = infoinput(55)
c
c ESC quits wherever we are
      if(ipress.eq.23) stop ' '
c F10 (defined as spare #26) returns to the starting directory, then quits
      if(ipress.eq.26) then
           stop ' '
        endif
c
      end
c
      subroutine ssort(files,nfil)
c
c Bubble sort of strings
c
      character*12 files(*),temp
c
      do 20, i=1,nfil-1
      do 10, j = i+1,nfil
      if(files(j).lt.files(i)) then
           temp = files(i)
           files(i) = files(j)
           files(j) = temp
        endif
 10   continue
 20   continue
      return
      end
