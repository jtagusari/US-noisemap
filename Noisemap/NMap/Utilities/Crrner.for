      subroutine corner(filnam,utmcor,izone)
c
c This routine opens a the specified DLG file and obtains, from the header,
c the UTM corners and zone.
c
c Input Argument:
c
c     FILNAM - Name of the DLG file, including the path to it. (char*20)
c
c              The file name itself is eight characters, of the form
c              aaallsss, where
c               aaa is a 3 character quadrangle (60'x30') identifier
c                   consisting of two letters and a number.  I haven't
c                   yet learned the exact pattern.
c               ll  is a two letter layer type designator (see below
c                   under CATNAM for types)
c               sss is a division designator, either Fnn (nn = 01-08) for
c                   low-density division or Snn (nn = 01-32) for high
c                   density
c             On USGS's UNIX system, the quads are named aaa.llsss.typ.gz,
c             where typ is either "std" or "opt" (two different data form-
c             ats) and gz denotes that the files are compressed via gnuzip.
c             This software deals only with standard format files.
c
c
c Output Arguments:
c
c     UTMCOR(4,2) - corners of the UTM area covered
c
c     IZONE - the UTM zone
c
      character*144 inline
      character*40 name
      character*16 flags
      character*12 filnam
      character*2 cornam(4)
      dimension icornr(4,2)
      dimension utmcor(4,2)
      double precision proj(15),resol,regpts(4,2),a(4)
c
      open(unit=1,file=filnam)
c
c Record A.1.  Title and some flags.
      read(1,1001)inline
 1001 format(a144)
c
      name = inline(1:40)
      flags = inline(129:144)
c
c Record A.2.  DLG level code, planimetric reference system, zone, and first
c five projection parameters.
c
      read(1,1002)levdlg,iplnrf,izone,(proj(i),i=1,5)
 1002 format(3i6,5d24.15)
c
c Record A.3. Next six projection parameters
c
      read(1,1003)(proj(i),i=6,11)
 1003 format(6d24.15)
c
c Record A.4.  Final four projection parameters, unit code, resolution,
c accuracy code, and number of sides in polygon.
c
      read(1,1004)(proj(i),i=12,15),iunits,resol,iaccur,nsides
 1004 format(4d24.15,i6,d24.15,2i6)
c
c Records A.5 and A.6.  Geographic coordinates of registration points.
c
      read(1,1005)regpts
 1005 format(6d24.15)
c
c Record B.1. Parameters for file coord to UTM conversion, number of reg-
c istration points.
c
      read(1,1006)a,mpts
 1006 format(4d24.15,i6)
c
c Record B.2.  File coordinates of the registration points.
c
      read(1,1007)(cornam(i),icornr(i,1),icornr(i,2),i=1,4)
 1007 format(4(a2,2i6))
c
c Compute the UTM values of the corners
      do 7, i = 1,4
      utmcor(i,1) = a(1)*float(icornr(i,1)) + a(2)*float(icornr(i,2))
     +              + a(3)
      utmcor(i,2) = a(1)*float(icornr(i,2)) - a(2)*float(icornr(i,1))
     +              + a(4)
 7    continue
c
      return
      end
c
      subroutine recorn(cornr,izon2,izone)
c
c This routine transposes UTM array cornr (4 pairs) from IZONE to IZON2.
c
      save
      dimension cornr(4,2)
      double precision east,north,seclat,seclon
      character*1 flag1,flag2
      data flag1,flag2 /2*' '/
      data ispher/0/
c
      if(izone.eq.izon2) return
      do 10, i = 1,4
           iz = izone
           iz2 = izon2
           east  = cornr(i,1)
           north = cornr(i,2)
           idlat = 0
           imlat = 0
           seclat = 0.
           idlon = 0
           imlon = 0
           seclon = 0.
           call utmsub(flag1,idlat,imlat,seclat,flag2,idlon,imlon,
     +             seclon,north,east,iz,ispher,iz2)
           cornr(i,1) = east
           cornr(i,2) = north
c
 10   continue
      return
      end
