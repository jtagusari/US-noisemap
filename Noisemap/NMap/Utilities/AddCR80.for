      program addcr80
c
c This program adds CRLF to an unformatted raw DLG file.  Line breaks
c are every 80 characters.
c
      character*80 inline
c
      open(unit=1,file=' ',form='binary',recl=80)
      open(unit=2,file=' ')
c
      n = 0
 100  continue
      read(1,end=110)inline
      l = len_trim(inline)
      l = max0(l,1)
      write(2,'(a)')inline(1:l)
      n = n+1
      go to 100
c
 110  close(1)
      close(2)
      write(*,*)n,' lines'
      stop ' '
      end
