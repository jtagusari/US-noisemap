
This directory contains the NMAP aircraft noise model,
a component of the Noisemap aircraft noise suite.  It 
contains NMap, the airport noise model, as well as Omega10 
and Omega11, utilities for predicting flyover and static 
spectral sound power levels for a user-supplied list of 
aircraft, power settings, and airspeeds.




 Files
-------

NMap.exe 

    The NMap executable program.  This program reads an input file 
    describing the flight operations of a number of aircraft, and computes the
    predicted noise impacts at a large number of points. 


Sample.opx
Sample.run
Sample.elv
Sample.imp

    NMap input files (case, run, elevation, and ground impedance files, 
    respectively) for a sample NMap case.  See the Documentation directory for 
    a description of the file formats.
    

Run Sample NMap Case.bat

    A batch file that runs the sample NMap case.  Simply double-click 
    on this file to run the sample case.


Omega10.exe
Flight01.dat
INM10SEL.dat

    Flight01.dat is a database of flyover spectral sound power levels for a 
    large sampling of aircraft, power settings, and airspeeds.  This database
    is sometimes referred to by the term NOISEFILE.
    
    INM10SEL.dat is a dataset of SEL noise levels verses distance for several 
    Integrated Noise Model (INM) aircraft.

    Omega10.exe is the Omega 10 executable program.  This utility reads a user's 
    list of aircraft/power setting/airspeed triplets, and produces a list of the 
    predicted flyover spectral sound power levels for each triplet by interpolating
    and/or extrapolating from the data in Flight01.dat and INM10SEL.dat.
    
    See the Documentation directory for a description of the format of Flight01.dat.


Omega11.exe
Static01.dat

    Static01.dat is a database of static spectral sound power levels for a 
    large sampling of aircraft and power settings.  This database is sometimes 
    referred to by the term NOISEFILE.

    Omega11.exe is the Omega 11 executable program.  This utility reads a user's 
    list of aircraft/power setting pairs, and produces a list of the predicted 
    static spectral sound power levels for each pair by interpolating and/or 
    extrapolating from the data in Static01.dat.

    See the Documentation directory for a description of the format of Static01.dat.


Sample.o10_input

    Omega10 input file for a sample Omega 10 case.  See the Documentation directory 
    for a description of the file format.
    

Run Sample Omega 10 Case.bat

    A batch file that runs the sample Omega 10 case.  Simply double-click 
    on this file to run the sample case.


Sample.o11_input

    Omega11 input file for a sample Omega 11 case.  See the Documentation directory 
    for a description of the file format.
    

Run Sample Omega 11 Case.bat

    A batch file that runs the sample Omega 11 case.  Simply double-click 
    on this file to run the sample case.


Old-to-New Aircraft Code Table.txt

    This file is used by BaseOps to import old-style NMap input files (i.e., 
    BPS files).  NMap uses a numerical code to indicate each type of aircraft.  
    The numerical codes were changed beginning with NMap 7.0.  This file 
    contains a mapping from the old codes to the new codes.  This allows 
    BaseOps to import old NMap input files.
    

Documentation\*

    User's manual for the NMap program, as well as documentation of the various 
    file formats read and written by NMap, Omega10, and Omega11.


Utilities\*

    A group of utilities that may be useful when working with NMap.  These
    are documented in the NMap User's Manual.



    
 Running NMap Manually 
-----------------------

Typically, you will run NMap from within BaseOps.  However, if desired, 
you can also run cases manually.  To do so, follow these steps:

    1. Change into the NMap directory.
    
    2. Run the command line...
    
           nmap filename.run
           
       ...where filename.run is the name of your case's .RUN file.




 Running Omega10 and Omega11 Manually
--------------------------------------

Typically, you will run Omega10 and Omega11 from within BaseOps.  
However, if desired, you can also run cases manually.  To do so, 
follow these steps:
    
    1. Change into the NMap directory.
    
    2. Run the command line...
    
           omega10 inputfilename logfilename noisefilename
        
       ...or
        
           omega11 inputfilename logfilename noisefilename
           
       ...where inputfilename is the name of the Omega 10 or Omega 11
       input file, logfilename is the name of a descriptive human-readable
       log file describing the calculations made, and noisefilename is
       a machine-readable file containing the predicted spectral sound
       power levels. 


